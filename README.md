# MioRobot

Cleaner version of the Arduino library for the Mio Robo3 robot.

The original is distributed by [miorobot.es](http://miorobot.es/formacion/linux-con-mblock)
as a [zip file](http://www.miorobot.es/mblock/miorobot.zip) that can be directly imported in the Arduino IDE.
This repository is the content of this zip file. For now only the source code was modified.

This project aims to make the code easier to read and possibly fix a few
issues. It comes with
[this article](https://vinvin.tf/projects/mio_programming.html) that explains
what this robot is and how to use it for education and fun.
