// miorobot.js

(function(ext) {
    var _device = null;
	var _util = null;
    var _rxBuf = [];

    // Sensor states:
    var ports = {
		OnBoard: 0,
        Port1: 1,
        Port2: 2,
        Port3: 3,
        Port4: 4,
		M1:    5,
		M2:    6
    };
	var angleXYZ={
		angleX:1,
		angleY:2,
		angleZ:3
	}
	var linefollowerSensors = {
		All:0,
		LeftSensor:1,
		MedianSensor:2,
		RightSensor:3,
		LeftLeftSensor:4,
		RightRightSensor:5
	};
	var linefolloweraddress = {

		LEFT:6,
		MIDDLE:8,
		RIGHT:10,
		LEFTLEFT:12,
		RIGHTRIGHT:14
	};
	var colornumbers = {

		RED:1,
		GREEN:2,
		BLUE:3
	};
	var audoAddress = {
		'music do':2,
		'music re':3,
		'music mi':4,
		'music fa':5,
		'music so':6,
		'music la':7,
		'music xi':8,
		'warm':9,
		'like':10,
		'curious':11,
		'suprised':12,
		'angry':13,

		'tired':15,
		'Mio!':16,
		'Doraemon':17,
		'London_Bridge':18,
        'Merry_Christmas':19,
        'Smurfs':20,
        'Stop':21
	};
	var IRbuttons = {
		'BUTTON_NULL':0,
		'BUTTON_SHUT_DOWN':1,
		'BUTTON_MENU':2,
		'BUTTON_SILENCE':3,
		'BUTTON_MODE':4,
		'BUTTON_PLUS':5,
		'BUTTON_RETURN':6,
		'BUTTON_FAST_BACKWARD':7,
		'BUTTON_PLAY_PAUSE':8,
		'BUTTON_FAST_FORWARD':9,
		'BUTTON_ZERO':10,
		'BUTTON_MINUS':11,
		'BUTTON_OK':12,
		'BUTTON_ONE':13,
		'BUTTON_TWO':14,
		'BUTTON_THREE':15,
		'BUTTON_FOUR':16,
		'BUTTON_FIVE':17,
		'BUTTON_SIX':18,
		'BUTTON_SEVEN':19,
		'BUTTON_EIGHT':20,
		'BUTTON_NINE':21,
	};
	var IRbuttonsROBO3 = {
		'BUTTON_NULL':0,
		'BUTTON_RED':1,
		'BUTTON_A':2,
		'BUTTON_GREEN':3,
		'BUTTON_B':4,
		'BUTTON_UP':5,
		'BUTTON_C':6,
		'BUTTON_LEFT':7,
		'BUTTON_OK':8,
		'BUTTON_RIGHT':9,
		'BUTTON_ZERO':10,
		'BUTTON_DOWN':11,
		'BUTTON_D':12,
		'BUTTON_ONE':13,
		'BUTTON_TWO':14,
		'BUTTON_THREE':15,
		'BUTTON_FOUR':16,
		'BUTTON_FIVE':17,
		'BUTTON_SIX':18,
		'BUTTON_SEVEN':19,
		'BUTTON_EIGHT':20,
		'BUTTON_NINE':21,
	}
	var switchStatus = {
		On:1,
		Off:0
	};
    var musicswitch = {
        On:1,
        Off:0
    };
	var slots = {
		Slot1:1,
		Slot2:2,
		Slot3:3
	};
	
	var LEVEL = {
		HIGH:1,
		LOW:0
	}; 
	var mp3state = {
		"STOP":14,
		"PLAY":1,
		"PAUS_PLAY":15,
		"LAST":4,
		"NEXT":3
    };
	
	 var mp3cycle = {
		"ALL_loop":0,
		"Folder_loop":1,
		"Single_loop":2,
		"Random_play":3,
		"Single_play":4
    };
	
	 var mp3eq = {
		"NO":0,
		"POP":1,
		"ROCK":2,
		"JAZZ":3,
		"CLASSIC":4,
		"BASS":5
    };
	 var mp3vol = {
		"vol+":5,
		"vol-":6
    };
	var flameSensors = {
		All:0,
		LeftSensor:1,
		MedianSensor:2,
		RightSensor:3,
		LeftLeftSensor:4,
		RightRightSensor:5
	};
	var flameaddress = {

		LEFT:6,
		MIDDLE:8,
		RIGHT:10,
		LEFTLEFT:12,
		RIGHTRIGHT:14
	};
	var key_numberROBO3 = {
		"UP":1,
		"DOWN":4,
		"LEFT":2,
		"RIGHT":3,
		"KEY_A":5,
		"KEY_B":6,
		"KEY_C":7,
		"KEY_D":8,
	};
	 var lcdcolor = {
		"BLACK":0,
		"RED":1,
		"GREEN":2,
		"BLUE":3,
		"YELLOW":4,
		"PURPLE":6,
		"GREY":7,
		"WHITE":16		
    };
	var lcdFill = {
		'off':0,
		'on':1
	};
	 var lcddirection = {
		"TRANSVERSE":1,
		"PORTRAIT":0,		
    };
	 var AI_Key_state = {
		"key_Press":1,
		"key_Release":0,	
    };
	 var AI_function = {
		"WeChat":1,
		"BaiduCloud":2,
		"VolSong+":3,
		"VolSong-":4,
		"B_W":5,
		"Configure":6,	
    };
/*
	var shutterStatus = {
		Press:1,
		Release:0,
		'Focus On':3,
		'Focus Off':2
	};
	var axis = {
		'X-Axis':1,
		'Y-Axis':2,
		'Z-Axis':3
	}
    var inputs = {
        slider: 0,
        light: 0,
        sound: 0,
        button: 0,
        'resistance-A': 0,
        'resistance-B': 0,
        'resistance-C': 0,
        'resistance-D': 0
    };
	*/
    function checkPortAndSlot(port, slot, sensor){
    	if((port == 7 || port == 8) && slot == 1){
			interruptThread(sensor + " not support Slot1 on Port" + port);
			return true;
		}
		return false;
    }
	var values = {};
	var indexs = [];
	var responsePreprocessor = {};
	var versionIndex = 0xFA;
    ext.resetAll = function(){
    	_device.send([0xff, 0x55, 2, 0, 4]);
    };
	ext.mio_runArduino = function(){
		responseValue();
	};
	ext.getButtonState= function(nextID, port, status){
		var deviceId = 0x12;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof status == "string"){
			if(status=="pressed"){
				status = 1;
			}else if(status=="released"){
				status = 0;
			}
		}
		getPackage(nextID,deviceId,port,status);
	}

	ext.mio_runMotor = function(port,speed) {
		if(typeof port=="string"){
			port = ports[port];
		}
        runPackage(10,port,short2array(speed));
    };
	ext.mio_run2Motors = function(speed1,speed2) {
		runPackage(10,0,short2array(speed1),short2array(speed2));
    };	
	ext.ExtendedMotor = function(port,speed1,speed2) {
		if(typeof port=="string"){
			port = ports[port];
		}
        runPackage(10,port,short2array(speed1),short2array(speed2));
    };
    ext.mio_runServo = function(port,slot,angle) {
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		if(angle > 180){
			angle = 180;
		}
		if(checkPortAndSlot(port, slot, "Servo")){
			return;
		}
        runPackage(11,port,slot,angle);
    };
	ext.mio_runAudio = function(port,address){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof address=="string"){
			address = audoAddress[address];
		}
		runPackage(14,port,address);
	};
	ext.mio_runLed = function(port,ledIndex,red,green,blue){
		ext.mio_runLedStrip(port, 0, ledIndex, red,green,blue);
	};
	ext.mio_runLine = function(port,ledIndex,red,green,blue){

		if(typeof port=="string"){
			port = slots[port];
		}
		runPackage(0x23,0,port,ledIndex=="all"?0:ledIndex,red,green,blue);
	};
	ext.mio_runDigital = function(port,slot,level) {
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		if(typeof level=="string"){
			level = LEVEL[level];
		}
        runPackage(0x15,port,slot,level);
    };
	ext.mio_runAnalog = function(port,slot,value) {
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
        runPackage(0x13,port,slot,value);
    };
	ext.mio_runLedStrip = function(port,slot,ledIndex,red,green,blue){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		if(checkPortAndSlot(port, slot, "Led strip")){
			return;
		}
		runPackage(8,port,slot,ledIndex=="all"?0:ledIndex,red,green,blue);
	};
	ext.mio_runLightsensor = function(port,status){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(3,port,switchStatus[status]);
	};
	ext.showNumber = function(port,number,brightness){
		if(typeof port=="string"){
			port = ports[port];
		}
		//runPackage(41,port,1,short2array(number),brightness);
		//runPackage(41,port,1,float2array(number),brightness);

		runPackage(41,port,1,short2array(number),brightness);
	};
	ext.showCharacters = function(port,message,column,brightness){
		if(typeof port=="string"){
			port = ports[port];
		}
		//var index = Math.max(0, Math.floor(x / -6));
		//message = message.toString().substr(index, 4);
        message = message.toString();
		runPackage(41,port,2,message.length,string2array(message),column,brightness);
	};

	ext.showDraw = function(port,color,brightness,bytes){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(41,port,4,bytes,brightness);
	};

	ext.showData = function(port,data,brightness){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(41,port,5,short2array(data),brightness);
	};
	ext.mio_runSegmentDisplay7 = function(port,number){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(0x09,port,float2array(number));
	};
	ext.setThreshold = function(port,address,value){
		if(typeof port=="string"){
			port = ports[port];
		}		
		if(typeof address=="string"){
			address = linefolloweraddress[address];
		}
		runPackage(17,port,address,short2array(value));
	};
	ext.set_music = function(sw){
		if(typeof sw=="string"){
			sw = musicswitch[sw];
		}
		runPackage(0x81,0,sw);
	};
	ext.mio_run_mp3_state = function(port,state){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof state=="string"){
			state = mp3state[state];
		}
		runPackage(0x21,port,state);
	};
	
	ext.mio_run_mp3_vol = function(port,vol){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof vol=="string"){
			vol = mp3vol[vol];
		}
		runPackage(0x21,port,vol);
	};
	
	ext.set_mp3_vol = function(port,vol_value){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(0x21,port,0x31,vol_value);
	};
	
	ext.mio_run_mp3_eq = function(port,eq){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof eq=="string"){
			eq = mp3eq[eq];
		}
		runPackage(0x21,port,0x32,eq);
	};
	
	ext.mio_run_mp3_cycle = function(port,cycle){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof cycle=="string"){
			cycle = mp3cycle[cycle];
		}
		runPackage(0x21,port,0x33,cycle);
	};
	
	ext.mio_run_mp3_choice = function(port,choice){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof choice=="string"){
			choice = mp3choice[choice];
		}
		runPackage(0x21,port,0x42,choice);
	};
	ext.lcd_clr = function(port,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x01,color);
	};
	ext.lcd_sd = function(port,x,y,n){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(0x20,port,0x02,short2array(x),short2array(y),n);
	};
	ext.lcd_hv = function(port,direction){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof direction=="string"){
			direction = lcddirection[direction];
		}
		runPackage(0x20,port,0x03,direction);
	};
	ext.lcd_bl = function(port,light){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(0x20,port,0x04,255-light);
	};
	ext.lcd_ps = function(port,x,y,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x05,short2array(x),short2array(y),color);
	};
	ext.lcd_pl = function(port,x1,y1,x2,y2,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x06,short2array(x1),short2array(y1),short2array(y2),short2array(x2),color);
	};
	ext.lcd_box = function(port,x1,y1,x2,y2,Fill,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		runPackage(0x20,port,0x07,short2array(x1),short2array(y1),short2array(x2),short2array(y2),color,Fill);
	};
	ext.lcd_cir = function(port,x,y,r,Fill,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		runPackage(0x20,port,0x08,short2array(x),short2array(y),short2array(r),color,Fill);
	};
	ext.lcd_sbc = function(port,color){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x09,color);
	};
	ext.lcd_num = function(port,num){
		if(typeof port=="string"){
			port = ports[port];
		}
		runPackage(0x20,port,0x0a,short2array(num));
	};
	ext.lcd_dcv16 = function(port,x,y,color,Fill,message){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x0b,short2array(x),short2array(y),color,Fill,string2array(message));
	};
	ext.lcd_dcv24 = function(port,x,y,color,Fill,message){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x0c,short2array(x),short2array(y),Fill,color,string2array(message));
	};
	ext.lcd_dcv32 = function(port,x,y,color,Fill,message){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x0d,short2array(x),short2array(y),Fill,color,string2array(message));
	};
	ext.lcd_dcv48 = function(port,x,y,color,Fill,message){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof Fill=="string"){
			Fill = lcdFill[Fill];
		}
		if(typeof color=="string"){
			color = lcdcolor[color];
		}
		runPackage(0x20,port,0x0e,short2array(x),short2array(y),Fill,color,string2array(message));
	};
	ext.set_AI = function(port,Function,Key_state){
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof Key_state=="string"){
			Key_state = AI_Key_state[Key_state];
		}
		if(typeof Function=="string"){
			Function = AI_function[Function];
		}
		runPackage(0x22,port,Function,Key_state);
	};
	ext.setflameThreshold = function(port,address,value){
		if(typeof port=="string"){
			port = ports[port];
		}		
		if(typeof address=="string"){
			address = flameaddress[address];
		}
		runPackage(0x18,port,address,short2array(value));
	};
	function runPackageForFace(){
		var bytes = [0xff, 0x55, 0, 0, 2];
		for(var i=0;i<arguments.length;i++){
			if(arguments[i].constructor == "[class Array]"){
				bytes = bytes.concat(arguments[i]);
			}else{
				bytes.push(arguments[i]);
			}
		}
		bytes[2] = bytes.length+13;
		_device.send(bytes);
	}
	var distPrev=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	var dist=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	var dist_output =[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
	ext.getUltrasonic = function(nextID,port){
		var deviceId = 1;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
	};

	ext.getDigitalState = function(nextID, port,slot, status){
		var deviceId = 0x15;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof status == "string"){
			if(status=="HIGH"){
				status = 1;
			}else if(status=="LOW"){
				status = 0;
			}
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		getPackage(nextID,deviceId,port,slot,status);
	}
	
	ext.getAnalog = function(nextID,port,slot) {
		var deviceId = 0x13;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		getPackage(nextID,deviceId,port,slot);
    };
	
	ext.getLinefollower = function(nextID,port,sensor) {
		var deviceId = 17;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof sensor=="string"){
			sensor = linefollowerSensors[sensor];
		}
		getPackage(nextID,deviceId,port,sensor);
    };
	ext.getLinefollowerthreshold = function(nextID,port,address) {
		var deviceId = 17;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof address=="string"){
			address = linefolloweraddress[address];
		}
		getPackage(nextID,deviceId,port,address);
	};
	ext.getflame = function(nextID,port,sensor) {
		var deviceId = 0x18;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof sensor=="string"){
			sensor = flameSensors[sensor];
		}
		getPackage(nextID,deviceId,port,sensor);
    };
	ext.getfFlamethreshold = function(nextID,port,address) {
		var deviceId = 0x18;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof address=="string"){
			address = flameaddress[address];
		}
		getPackage(nextID,deviceId,port,address);
	};
	ext.getGarysensor = function(nextID,port) {
		var deviceId = 0x19;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
    };
	ext.getkeyValue = function(nextID,port) {
		var deviceId = 0X17;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port,0);
    };	
	ext.JudgekeyValue = function(nextID,port,key_number) {
		var deviceId = 0X17;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof key_number=="string"){
			key_number = key_numberROBO3[key_number];
		}
		getPackage(nextID,deviceId,port,key_number);
    };
	ext.getPirmotion = function(nextID, port){
		var deviceId = 0x16;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
	}
	ext.getTEMP = function(nextID,port,slot) {
		var deviceId = 0x0d;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof slot=="string"){
			slot = slots[slot];
		}
		getPackage(nextID,deviceId,port,slot);
    };
	ext.getLightsensor = function(nextID,port) {
		var deviceId = 3;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
    };
	ext.getColorsensor = function(nextID,port,colornumber) {
		var deviceId = 0X14;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof colornumber=="string"){
			colornumber = colornumbers[colornumber];
		}
		getPackage(nextID,deviceId,port,colornumber);
    };
	ext.getSoundsensor = function(nextID,port) {
		var deviceId = 7;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
    };
	ext.getPOENTIOMETER = function(nextID,port) {
		var deviceId = 0x04;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
    };
	ext.getMPU6050 = function(nextID,port,angle) {
		var deviceId = 0x05;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof angle=="string"){
			angle = angleXYZ[angle];
		}
		getPackage(nextID,deviceId,port,angle);
    };
	ext.getInfrared = function(nextID,port,IRbutton) {
		var deviceId = 0x10;
		if(typeof port=="string"){
			port = ports[port];
		}
		if(typeof IRbutton=="string"){
			IRbutton = IRbuttonsROBO3[IRbutton];
		}
		getPackage(nextID,deviceId,port,IRbutton);
    };
	ext.getInfraredValue = function(nextID,port) {
		var deviceId = 16;
		if(typeof port=="string"){
			port = ports[port];
		}
		getPackage(nextID,deviceId,port);
    };
//ext.getBatVoltage = function(nextID) {
//	var deviceId = 0x0f;
//	getPackage(nextID,deviceId);
// };

	function sendPackage(argList, type){
		var bytes = [0xff, 0x55, 0, 0, type];
		for(var i=0;i<argList.length;++i){
			var val = argList[i];
			if(val.constructor == "[class Array]"){
				bytes = bytes.concat(val);
			}else{
				bytes.push(val);
			}
		}
		bytes[2] = bytes.length - 3;
		_device.send(bytes);
	}
	
	function runPackage(){
		sendPackage(arguments, 2);
	}
	function getPackage(){
		var nextID = arguments[0];
		Array.prototype.shift.call(arguments);
		sendPackage(arguments, 1);
	}
    
    var inputArray = [];
	var _isParseStart = false;
	var _isParseStartIndex = 0;
    function processData(bytes) {
		var len = bytes.length;
		if(_rxBuf.length>30){
			_rxBuf = [];
		}
		for(var index=0;index<bytes.length;index++){
			var c = bytes[index];
			_rxBuf.push(c);
			if(_rxBuf.length>=2){
				if(_rxBuf[_rxBuf.length-1]==0x55 && _rxBuf[_rxBuf.length-2]==0xff){
					_isParseStart = true;
					_isParseStartIndex = _rxBuf.length-2;
				}
				if(_rxBuf[_rxBuf.length-1]==0xa && _rxBuf[_rxBuf.length-2]==0xd&&_isParseStart){
					_isParseStart = false;
					
					var position = _isParseStartIndex+2;
					var extId = _rxBuf[position];
					position++;
					var type = _rxBuf[position];
					position++;
					//1 byte 2 float 3 short 4 len+string 5 double
					var value;
					switch(type){
						case 1:{
							value = _rxBuf[position];
							position++;
						}
							break;
						case 2:{
							value = readFloat(_rxBuf,position);
							position+=4;
						}
							break;
						case 3:{
							value = readInt(_rxBuf,position,2);
							position+=2;
						}
							break;
						case 4:{
							var l = _rxBuf[position];
							position++;
							value = readString(_rxBuf,position,l);
						}
							break;
						case 5:{
							value = readDouble(_rxBuf,position);
							position+=4;
						}
							break;
						case 6:
							value = readInt(_rxBuf,position,4);
							position+=4;
							break;
					}
					if(type<=6){
						if (responsePreprocessor[extId] && responsePreprocessor[extId] != null) {
							value = responsePreprocessor[extId](value);
							responsePreprocessor[extId] = null;
						}
						responseValue(extId,value);
					}else{
						responseValue();
					}
					_rxBuf = [];
				}
			} 
		}
    }
	
	function readFloat(arr,position){
		var f= [arr[position],arr[position+1],arr[position+2],arr[position+3]];
		return parseFloat(f);
	}
	function readInt(arr,position,count){
		var result = 0;
		for(var i=0; i<count; ++i){
			result |= arr[position+i] << (i << 3);
		}
		return result;
	}
	function readDouble(arr,position){
		return readFloat(arr,position);
	}
	function readString(arr,position,len){
		var value = "";
		for(var ii=0;ii<len;ii++){
			value += String.fromCharCode(_rxBuf[ii+position]);
		}
		return value;
	}

    // Extension API interactions
    var potentialDevices = [];
    ext._deviceConnected = function(dev) {
         potentialDevices.push(dev);
        if (!_device) {
            tryNextDevice();
        }
    }

    function tryNextDevice() {
        // If potentialDevices is empty, device will be undefined.
        // That will get us back here next time a device is connected.
        _device = potentialDevices.shift();
        if (_device) {
            _device.open({ stopBits: 0, bitRate: 115200, ctsFlowControl: 0 }, deviceOpened);
        }
    }


    var watchdog = null;
     function deviceOpened(dev) {
        if (!dev) {
            // Opening the port failed.
    //        tryNextDevice();
            return;
        }
     //   _device.set_receive_handler('Mio Robot',processData);
    };

    ext._deviceRemoved = function(dev) {
        if(_device != dev) return;
        _device = null;
    };

    ext._shutdown = function() {
        if(_device) _device.close();
        _device = null;
    };

    ext._getStatus = function() {
        if(!_device) return {status: 1, msg: 'Mio disconnected'};
        if(watchdog) return {status: 1, msg: 'Probing for Mio'};
        return {status: 2, msg: 'Mio connected'};
    }

    var descriptor = {};
	ScratchExtensions.register('Mio Robot', descriptor, ext, {type: 'serial'});
})({});
