#include "ROBO3_SoundSensor.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_SoundSensor soundSensor;

void setup()
{
    soundSensor.setpin_mio(onBoardPort);
    Serial.begin(115200);
}

void loop()
{
    Serial.print("sound sensor value: ");
    Serial.println(soundSensor.read());
    delay(50);
}
