#include <EEPROM.h>
#include <Wire.h> 
#include <Servo.h>
#include "ROBO3_130Motor.h"

ROBO3_130Motor Motor_130;
uint32_t time_test = millis();
void setup() {
  // put your setup code here, to run once:
  pinMode(9, OUTPUT);
  Motor_130.setpin_mio(1,1);
}

void loop() {
  // put your main code here, to run repeatedly:
    digitalWrite(9, LOW);
    Motor_130.run(30);
    delay(2000);
    digitalWrite(9,HIGH);
    Motor_130.run(50);
    delay(2000);
}
