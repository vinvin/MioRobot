#include "ROBO3_Flame.h"
#include "ROBO3_InfraredReceive.h"
#include "ROBO3_RGBled.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_Flame Flame;
ROBO3_InfraredReceive ir;
ROBO3_RGBled RGBled;
void setup() {
  // put your setup code here, to run once:
  Flame.setpin_mio(1);
    ir.setpin_mio(onBoardPort);
    RGBled.setpin_mio(onBoardPort);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  ir.checkSignal();
    if(ir.read())
    { 
      RGBled.setColor(0, 100, 0, 0);//set RGB value to one LED
      RGBled.show();
      switch(ir.read())
      {
        case 3:Flame.set_flame_value(6,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 4:Flame.set_flame_value(8,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 5:Flame.set_flame_value(10,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 6:Flame.set_flame_value(12,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 7:Flame.set_flame_value(14,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        
        case 13:Flame.set_flame_value(6,75);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 14:Flame.set_flame_value(8,125);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 15:Flame.set_flame_value(10,200);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 16:Flame.set_flame_value(12,225);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 17:Flame.set_flame_value(14,250);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        
        case 18:Flame.set_flame_value(15,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 19:Flame.set_flame_value(15,175);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 20:Flame.set_flame_value(15,125);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
      }
      while(1)
      {
          ir.checkSignal();
          if(ir.read()==0) 
          {
            RGBled.setColor(0, 0, 0, 0);//set RGB value to one LED
            RGBled.show();
            break;
          }
      }
    }
    Serial.print(" Flame sensor value: ");
    Serial.print(Flame.read(0));
    Serial.print(" sensor1 value: ");
    Serial.print(Flame.read(1));
    Serial.print(" sensor2 value: ");
    Serial.print(Flame.read(2));
    Serial.print(" sensor3 value: ");
    Serial.print(Flame.read(3));
    Serial.print(" sensor4 value: ");
    Serial.print(Flame.read(4));
    Serial.print(" sensor5 value: ");
    Serial.println(Flame.read(5));
    Serial.print("  ");
    Serial.print(" ");
    Serial.print(Flame.read(6));
    Serial.print(" ");
    Serial.print(Flame.read(8));
    Serial.print(" ");
    Serial.print(Flame.read(10));
    Serial.print(" ");
    Serial.print(Flame.read(12));
    Serial.print(" ");
    Serial.print(Flame.read(14));
    Serial.println(" ");
    delay(100);
}
ISR(PCINT2_vect)
{
    ir.dataUpdate();
}
