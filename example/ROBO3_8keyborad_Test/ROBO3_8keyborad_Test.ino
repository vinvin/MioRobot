#include "ROBO3_Key.h"
#include <EEPROM.h>
#include <Wire.h>
 #include <Servo.h>
ROBO3_Key KEY;

void setup()
{
    KEY.setpin_mio(4);
    Serial.begin(115200);
}

void loop()
{
    Serial.print(KEY.read(0));
    Serial.print("  ");
    Serial.print(KEY.read(1));
    Serial.print("  ");
    Serial.print(KEY.read(2));
    Serial.print("  ");
    Serial.print(KEY.read(3));
    Serial.print("  ");
    Serial.print(KEY.read(4));
    Serial.print("  ");
    Serial.print(KEY.read(5));
    Serial.print("  ");
    Serial.print(KEY.read(6));
    Serial.print("  ");
    Serial.print(KEY.read(7));
    Serial.print("  ");
    Serial.println(KEY.read(8));
    delay(50);
}
