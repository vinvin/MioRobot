#include <ROBO3_JOYSTICK.h>
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_JOYSTICK JOY;

void setup()
{
    JOY.setpin_mio(2);
    Serial.begin(115200);
}

void loop()
{
  Serial.print(JOY.read(1));
  Serial.print("   ");
  Serial.print(JOY.read(2));
  Serial.println("   ");
}
