#include "ROBO3_DCMotorTC118S.h"
#include <EEPROM.h>
#include <Wire.h> 
#include <Servo.h>
ROBO3_DCMotorTC118S motor1;
ROBO3_DCMotorTC118S motor2;
ROBO3_DCMotorTC118S motor_RJ25;

void setup()
{
    motor1.setpin_mio(MIO_M1);
    motor2.setpin_mio(MIO_M2);
    motor_RJ25.setpin_mio(1);
    
}

void loop()
{
    motor1.run(255);
    motor2.run(255);
    motor_RJ25.run(255,255);
    delay(1000);
    motor1.run(-255);
    motor2.run(-255);
    motor_RJ25.run(-255,-255);
    delay(1000);
}
