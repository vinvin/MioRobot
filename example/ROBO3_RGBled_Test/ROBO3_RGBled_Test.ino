//#include "ROBO3_MioBoard.h"
//#include "ROBO3_ModuleProtocol.h"
//#include "ROBO3_Config.h"
#include "ROBO3_RGBled.h"
#include <EEPROM.h>
#include <Wire.h>
ROBO3_RGBled RGBled;
ROBO3_RGBled RGBled_board;

float i, j, k,num=0;
uint32_t times = millis();
void setup() {
  // put your setup code here, to run once:
  RGBled.setpin_mio(1);
  RGBled_board.setpin_mio(onBoardPort);
}

void loop() {
  // put your main code here, to run repeatedly:
  for(uint8_t t = 0; t < 5; t++)
    {
        uint8_t red  = 64 * (1 + sin(t / 2.0 + i / 4.0) );//calculate red value
        uint8_t green = 64 * (1 + sin(t / 1.0 + j / 9.0 + 2.1) );//calculate green value
        uint8_t blue = 64 * (1 + sin(t / 3.0 + k / 14.0 + 4.2) );//calculate blue value
        RGBled_board.setColorAt(t, red, green, blue);//set RGB value to one LED
        
    }
    RGBled_board.show();
    i += random(1, 6) / 6.0;
    j += random(1, 6) / 6.0;
    k += random(1, 6) / 6.0;
    delay(5);
    if((millis()-times)>300)
    {
      times = millis();
      num= random(0, 5);
      RGBled.setColor(num, num*20, num*40, num*80);//set RGB value to one LED
      RGBled.show();
    }
}
