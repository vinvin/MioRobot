#include <ROBO3_MPU6050.h>
#include <EEPROM.h>
#include <Wire.h>

ROBO3_MPU6050 MPU6050;
uint8_t buffer_value[12]={0};
uint8_t flag = 1;//flag=1 读取角度 角速度 加速度 flag=0 读取12个字节原始数据
void setup()
{
    MPU6050.setpin_mio(1);
    Serial.begin(115200);
}



void loop()
{
  if(flag)
  {
    for(uint8_t i =0 ;i<9;i++)
    {
        Serial.print(MPU6050.read(i)); 
        Serial.print("   ");
    }
  }
  else
  {
    MPU6050.read_buffer(buffer_value);
    for(uint8_t i =0 ;i<12;i++)
    {
        Serial.write(buffer_value[i]); 
        Serial.print("   ");
    }
  }


      Serial.println("   "); 
       
}  
