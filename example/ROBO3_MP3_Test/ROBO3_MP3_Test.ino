#include <Servo.h>
#include <Arduino.h>
#include <EEPROM.h> 
#include <Wire.h>
#include "ROBO3_MP3.h"
#include "ROBO3_InfraredReceive.h"

ROBO3_InfraredReceive ir;
ROBO3_MP3 MP3;

uint8_t ir_data =0;
void setup() {
  // put your setup code here, to run once:
    MP3.setpin_mio(1);
    ir.setpin_mio(onBoardPort);
}

void loop() {
  // put your main code here, to run repeatedly:
  ir.checkSignal();
  ir_data =ir.read();
  if(ir_data!=0)
  {
    switch(ir_data)
    {
      case 1:MP3.MP3_STATE(STOP);break;
      case 12:MP3.MP3_STATE(PLAY);break;
      case 8:MP3.MP3_STATE(PAUS_PLAY);break;
      case 7:MP3.MP3_STATE(LAST);break;
      case 9:MP3.MP3_STATE(NEXT); break;
      case 5:MP3.MP3_STATE(VOL_UP); break;
      case 11:MP3.MP3_STATE(VOL_DOWN); break;
      
      case 2:MP3.SET_MP3_MODE(VOL,30); break;
      case 3:MP3.SET_MP3_MODE(VOL,0); break;
      case 4:MP3.SET_MP3_MODE(VOL,15); break;
      case 16:MP3.SET_MP3_MODE(CYCLE,1); break;
      case 17:MP3.SET_MP3_MODE(CYCLE,4); break;
      case 18:MP3.SET_MP3_MODE(CYCLE,2); break;
      case 19:MP3.SET_MP3_MODE(EQ,1); break;
      case 20:MP3.SET_MP3_MODE(EQ,4); break;
      case 21:MP3.SET_MP3_MODE(EQ,2); break;
      
      case 14:MP3.MP3_CHOICE(1,0x44,3); break;
      case 15:MP3.MP3_CHOICE(1,0x42,3); break;
    }
    while(1)
    {
        ir.checkSignal();
        ir_data =ir.read();
        if(ir_data==0)
        break;
    }
  }
}

ISR(PCINT0_vect)
{
    ir.dataUpdate();
}
ISR(PCINT1_vect)
{
    ir.dataUpdate();
}
ISR(PCINT2_vect)
{
    ir.dataUpdate();
}
