#include "ROBO3_Button.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_Button btn;
ROBO3_Button btn_RJ25;

void setup()
{
    btn.setpin_mio(onBoardPort);
    btn_RJ25.setpin_mio(2);
    Serial.begin(115200);
}

void loop()
{
    if(btn_RJ25.read()||btn.read())
        Serial.println("button is pressed");
    else
        Serial.println("button is released");
    delay(50);
}
