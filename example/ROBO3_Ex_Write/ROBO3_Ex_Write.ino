#include <EEPROM.h>
#include <Wire.h>
#include <Servo.h>
#include "ROBO3_Extern.h"  
  
ROBO3_Extern Extern;
 void setup() {
  Extern.setpin_mio(1);
  Serial.begin(115200);
}

void loop() {
//占空比满幅是255
Extern.analogWrite(1,64);//插头1  占空比四分之一
Extern.analogWrite(2,128);//插头2  占空比二分之一
Extern.analogWrite(3,192);//插头3  占空比三分之二
delay(1000);
Extern.analogWrite(1,0);//插头1  占空比四分之一
Extern.analogWrite(2,0);//插头2  占空比二分之一
Extern.analogWrite(3,0);//插头3  占空比三分之二
delay(1000);
Extern.digitalWrite(1,1);//插头1  占空比四分之一
Extern.digitalWrite(2,1);//插头2  占空比二分之一
Extern.digitalWrite(3,1);//插头3  占空比三分之二
delay(1000);
Extern.digitalWrite(1,0);//插头1  占空比四分之一
Extern.digitalWrite(2,0);//插头2  占空比二分之一
Extern.digitalWrite(3,0);//插头3  占空比三分之二
delay(1000);
}
