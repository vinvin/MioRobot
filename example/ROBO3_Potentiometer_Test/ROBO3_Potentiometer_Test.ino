#include <EEPROM.h>
#include <Wire.h>
#include <ROBO3_Potentiometer.h>
ROBO3_Potentiometer Pot;

void setup() {
  // put your setup code here, to run once:
  Pot.setpin_mio(1);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  //Pot.read();
  Serial.print("  PotentiometerValue is ");
  Serial.println(Pot.read());
  delay(100);
}
