#include "ROBO3_LineFollower.h"
#include "ROBO3_InfraredReceive.h"
#include "ROBO3_RGBled.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_LineFollower linefollower;
ROBO3_LineFollower linefollower5;
ROBO3_InfraredReceive ir;
ROBO3_RGBled RGBled;

void setup()
{
    linefollower5.setpin_mio(1);
    ir.setpin_mio(onBoardPort);
    RGBled.setpin_mio(onBoardPort);
    Serial.begin(115200);
}

void loop()
{
//    Serial.print(" line follower sensor value: ");
//    Serial.print(linefollower.read(ALL_SENSORS));
    Serial.print(linefollower5.read(0));
    Serial.print(" ");
    Serial.print(" ");
for(uint8_t i =1;i<6;i++)
{
    //Serial.print(i);
    Serial.print(linefollower5.read(i));
    Serial.print(" ");
}
    Serial.print(" ");
for(uint8_t i =6;i<15;i=i+2)
{
   // Serial.print(i);
    Serial.print(linefollower5.read(i));
    Serial.print(" ");
}
    Serial.println(" ");
ir.checkSignal();
    if(ir.read())
    { 
      RGBled.setColor(0, 100, 0, 0);//set RGB value to one LED
      RGBled.show();
      switch(ir.read())
      {
        case 3:linefollower5.set_light_value(6,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 4:linefollower5.set_light_value(8,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 5:linefollower5.set_light_value(10,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 6:linefollower5.set_light_value(12,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 7:linefollower5.set_light_value(14,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        
        case 13:linefollower5.set_light_value(6,75);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 14:linefollower5.set_light_value(8,125);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 15:linefollower5.set_light_value(10,200);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 16:linefollower5.set_light_value(12,225);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 17:linefollower5.set_light_value(14,250);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        
        case 18:linefollower5.set_light_value(15,0);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 19:linefollower5.set_light_value(15,175);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
        case 20:linefollower5.set_light_value(15,125);RGBled.setColor(0, 100, 0, 0);RGBled.show();break;
      }
      while(1)
      {
          ir.checkSignal();
          if(ir.read()==0) 
          {
            RGBled.setColor(0, 0, 0, 0);//set RGB value to one LED
            RGBled.show();
            break;
          }
      }
    }
}
ISR(PCINT2_vect)
{
    ir.dataUpdate();
}
