#include "ROBO3_ColorSensor.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_ColorSensor ColorSensor;
void setup() {
  // put your setup code here, to run once:
  ColorSensor.setpin_mio(1);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
    Serial.print(" RED sensor value: ");
    Serial.print(ColorSensor.read(RED));

    Serial.print(" GREEN sensor value: ");
    Serial.print(ColorSensor.read(GREEN));

    Serial.print(" BLUE sensor value: ");
    Serial.println(ColorSensor.read(BLUE));
    delay(100);
}
