#include "ROBO3_SegmentDisplay7.h"
#include <Wire.h>
#include <EEPROM.h>

ROBO3_SegmentDisplay7  digitron;
float a;
void setup() {
  digitron.setpin_mio(1);

}

void loop() {
digitron.showNumber(-0.123);
delay(500);
digitron.showNumber(-1.23);
delay(500);
digitron.showNumber(-12.3);
delay(500);
digitron.showNumber(-123);
delay(500);
digitron.showNumber(-1234);
delay(500);
digitron.showNumber(0.1234);
delay(500);
digitron.showNumber(1.2345);
delay(500);
digitron.showNumber(12.345);
delay(500);
digitron.showNumber(123.45);
delay(500);
digitron.showNumber(1234.5);
delay(500);
digitron.showNumber(12345);
delay(500);
}
