#include "ROBO3_LightSensor.h"
#include <EEPROM.h>
#include <Wire.h>
ROBO3_LightSensor lightSensor;
ROBO3_LightSensor lightSensor_RJ25;

void setup()
{
    lightSensor.setpin_mio(onBoardPort);
    lightSensor_RJ25.setpin_mio(1);
    Serial.begin(115200);
}

void loop()
{
    Serial.print("light sensor value: ");
    Serial.print(lightSensor.read());
   Serial.print("  light sensor RJ25 value: ");
   Serial.println(lightSensor_RJ25.read());
    delay(50);
}
