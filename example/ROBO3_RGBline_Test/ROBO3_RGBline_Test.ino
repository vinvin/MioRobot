#include "ROBO3_RGBline.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_RGBline RGBline3;
ROBO3_RGBline RGBline2;
ROBO3_RGBline RGBline1;

#define DELAY_TIME  300
float i, j, k,num;
void setup()
{
    RGBline1.setpin_mio(1,1);
    RGBline2.setpin_mio(1,2);
    RGBline3.setpin_mio(1,3);
}

void loop()
{

    for(uint8_t t = 1; t < 10; t++)
    {
      RGBline2.setColor(t, 255, 255, 255);//set RGB value to one LED
      RGBline2.show();
     delay(50);
      RGBline1.setColor(t, 255, 255, 255);//set RGB value to one LED
      RGBline1.show();
     delay(50);
      RGBline3.setColor(t, 255, 255, 255);//set RGB value to one LED
      RGBline3.show();
      delay(DELAY_TIME/6);
    }    
      RGBline2.setColor(0, 0, 0, 0);//set RGB value to one LED
      RGBline2.show();
     delay(50);
      RGBline1.setColor(0, 0, 0, 0);//set RGB value to one LED
      RGBline1.show();
     delay(50);
      RGBline3.setColor(0, 0, 0, 0);//set RGB value to one LED
      RGBline3.show();
      delay(DELAY_TIME/6);
//    for(uint8_t t = 9; t > 0; t--)
//    {
//      RGBline2.setColor(t, 0, 0, 0);//set RGB value to one LED
//      RGBline2.show();
//     delay(50);
//      RGBline1.setColor(t, 0, 0, 0);//set RGB value to one LED
//      RGBline1.show();
//     delay(50);
//      RGBline3.setColor(t, 0, 0, 0);//set RGB value to one LED
//      RGBline3.show();
//      delay(DELAY_TIME/6);
//    }
     RGBline2.setColor(0, 255, 0, 0);//set RGB value to one LED
     RGBline2.show();
     delay(50);
     RGBline1.setColor(0, 255, 0, 0);//set RGB value to one LED
     RGBline1.show();
     delay(50);
     RGBline3.setColor(0, 255, 0, 0);//set RGB value to one LED
     RGBline3.show();
     delay(DELAY_TIME);
     RGBline2.setColor(0, 0, 255, 0);//set RGB value to one LED
     RGBline2.show();
     delay(50);
     RGBline1.setColor(0, 0, 255, 0);//set RGB value to one LED
     RGBline1.show();
     delay(50);
     RGBline3.setColor(0, 0, 255, 0);//set RGB value to one LED
     RGBline3.show();
     delay(DELAY_TIME);
     RGBline2.setColor(0, 0, 0, 255);//set RGB value to one LED
     RGBline2.show();
     delay(50);
     RGBline1.setColor(0, 0, 0, 255);//set RGB value to one LED
     RGBline1.show();
     delay(50);
     RGBline3.setColor(0, 0, 0, 255);//set RGB value to one LED
     RGBline3.show();
     delay(DELAY_TIME);
     RGBline2.setColor(0, 0, 0, 0);//set RGB value to one LED
     RGBline2.show();
     delay(50);
     RGBline1.setColor(0, 0, 0, 0);//set RGB value to one LED
     RGBline1.show();
     delay(50);
     RGBline3.setColor(0, 0, 0, 0);//set RGB value to one LED
     RGBline3.show();
     delay(DELAY_TIME);
//    for(uint8_t t = 0; t < 50; t++)
//    {
//        uint8_t red  = 64 * (1 + sin(t / 2.0 + i / 4.0) );//calculate red value
//        uint8_t green = 64 * (1 + sin(t / 1.0 + j / 9.0 + 2.1) );//calculate green value
//        uint8_t blue = 64 * (1 + sin(t / 3.0 + k / 14.0 + 4.2) );//calculate blue value
//        RGBline2.setColorAt(t, red, green, blue);//set RGB value to one LED
//        
//    }
//    RGBline2.show();
//    delay(500);
}
