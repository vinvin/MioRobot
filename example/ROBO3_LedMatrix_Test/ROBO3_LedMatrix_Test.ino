#include "ROBO3_LedMatrix.h"
#include <EEPROM.h>
#include <Wire.h>
 #include <Servo.h>
ROBO3_LedMatrix leds;

uint8_t num=0;
uint8_t ledBuffer[16]={0x00, 0x4c, 0x52, 0x52, 0xde, 0x5e, 0x0c, 0x00, 0x00, 0x4c, 0x52, 0x52, 0xde, 0x5e, 0x0c, 0x00};

void setup()
{
    leds.setpin_mio(1);
}

void loop()
{
//    leds.showNumber(num++,4);
//    delay(500);
//    leds.showString("Hi",3,4);
//    delay(500);
    leds.showBitMap(ledBuffer,4);
    delay(500);
}
