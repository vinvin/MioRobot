#include "ROBO3_InfraredReceive.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_InfraredReceive ir;

void setup()
{
    ir.setpin_mio(onBoardPort);
    Serial.begin(115200);
}

void loop()
{
    ir.checkSignal();
    Serial.print("remote button is: ");
    Serial.println(ir.read());
    delay(50);
}

ISR(PCINT0_vect)
{
    ir.dataUpdate();
}
ISR(PCINT1_vect)
{
    ir.dataUpdate();
}
ISR(PCINT2_vect)
{
    ir.dataUpdate();
}
