#include "ROBO3_Gray.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_Gray Gray;

void setup()
{
    Gray.setpin_mio(1);
    Serial.begin(115200);
}

void loop()
{
    Serial.print("Gray value: ");
    Serial.println(Gray.read());
    delay(100);
}
