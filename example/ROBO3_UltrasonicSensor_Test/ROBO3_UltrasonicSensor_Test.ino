#include "ROBO3_UltrasonicSensor.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
ROBO3_UltrasonicSensor us;

void setup()
{
    us.setpin_mio(1);
    Serial.begin(115200);
}

void loop()
{
    Serial.print("ultrasonic sensor value: ");
    Serial.print(us.read());
    Serial.println(" cm");
    delay(50);
}
