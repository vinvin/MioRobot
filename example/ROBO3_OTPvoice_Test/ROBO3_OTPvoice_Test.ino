#include "ROBO3_OTPvoice.h"
#include <Servo.h>
#include <EEPROM.h>
#include <Wire.h>
#define BEAT_TIME  200
ROBO3_OTPvoice otp;

void setup()
{
    otp.setpin_mio(onBoardPort);
}

void loop()
{
    otp.write(MUSIC_DO);
    delay(BEAT_TIME);
    otp.write(MUSIC_RE);
    delay(BEAT_TIME);
    otp.write(MUSIC_MI);
    delay(BEAT_TIME);
    otp.write(MUSIC_FA);
    delay(BEAT_TIME);
    otp.write(MUSIC_SO);
    delay(BEAT_TIME);
    otp.write(MUSIC_LA);
    delay(BEAT_TIME);
    otp.write(MUSIC_XI);
    delay(BEAT_TIME*10);
}
