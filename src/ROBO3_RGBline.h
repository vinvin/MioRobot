#ifndef ROBO3_RGBLINE_H
#define ROBO3_RGBLINE_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

#define DEFAULT_MAX_LED_LINE_NUMBER  (20)

#define SET_NUMBER          1
#define TURN_ON             2

/* Accessing the additional LED ribbon? What's that? */

class ROBO3_RGBline : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_RGBline(void);
		~ROBO3_RGBline(void);
		void setpin_arduino(uint8_t port);
		void setpin_mio(uint8_t port, uint8_t num);
		void setColorAt(uint8_t index, uint8_t red, uint8_t green, uint8_t blue);
		void setColor(uint8_t index, uint8_t red, uint8_t green, uint8_t blue);
		void setNumber(uint8_t ledNum);
		void show();
		uint8_t getPort();

	private:
		uint16_t count_led;
		uint8_t *pixels;
		void rgbled_sendarray_mask(uint8_t *array, uint16_t length, uint8_t pinmask, uint8_t *port);
		const volatile uint8_t *ws2812_port;
		volatile uint8_t *ws2812_port_reg;
		uint8_t pinMask;
		uint8_t _port;
		uint8_t Coulor_Group[6];
		uint8_t RGBline_Pin;
		uint8_t RGB_SLOT;
};

#endif
