#ifndef ROBO3_SOUNDSENSOR_H
#define ROBO3_SOUNDSENSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

/* Accessing the onboard sound level sensor of MIO */

class ROBO3_SoundSensor : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_SoundSensor(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint16_t read(void);

	private:
		uint8_t _sensorPin;
		uint8_t _port;
};

#endif
