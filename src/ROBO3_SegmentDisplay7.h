#ifndef ROBO3_SegmentDisplay7_H
#define ROBO3_SegmentDisplay7_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

/* Accessing the additional 7-segment displays
 * http://miorobot.es/formacion/display-numerico
 */

union {
	byte byteVal[4];
	int intVal[2];
	float floatVal;
	long longVal;
} val_7;

class ROBO3_SegmentDisplay7 : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_SegmentDisplay7(void);
		void setpin_mio(uint8_t port);
		void showNumber(float number);
	private:
		uint8_t _port;
};

#endif
