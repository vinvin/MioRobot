#ifndef ROBO3_DCMOTORTC118S_H
#define ROBO3_DCMOTORTC118S_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

/* One of the two motors provided with MIO, has an encoder and is compatible
 * with Lego */

class ROBO3_DCMotorTC118S : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_DCMotorTC118S(void);
		void setpin_arduino(uint8_t dirPin,uint8_t pwmPin);
		void setpin_mio(uint8_t port);
		void run(int16_t speed);
		void run(int16_t speed1,int16_t speed2);
		//void run(uint8_t motor_number,int16_t speed);
	private:
		uint8_t _port;
		uint8_t _INA_pin;
		uint8_t _INB_pin;
};

#endif
