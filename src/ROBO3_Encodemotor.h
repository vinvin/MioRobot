#ifndef ROBO3_ENCODEMOTOR_H
#define ROBO3_ENCODEMOTOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

class ROBO3_Encodemotor : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Encodemotor(void);
		void setpin_mio(uint8_t port);
		void run_pwm(int16_t pwm1, int16_t pwm2);
		void run_speed(int16_t speed1, int16_t speed2);
		void run_angle(int16_t angle1, int16_t speed1, int16_t angle2, int16_t speed2);
		int16_t read_speed(uint8_t motor);
	private:
		uint8_t _port;
		uint8_t _INA_pin;
		uint8_t _INB_pin;
};

#endif
