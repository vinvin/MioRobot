#include "ROBO3_Extern.h"

ROBO3_Extern::ROBO3_Extern(void)
{

	ROBO3_ModuleProtocol::init();
}

void ROBO3_Extern::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;

	ROBO3_ModuleProtocol::init();
}


uint8_t ROBO3_Extern::digitalRead(uint8_t Pin)
{
	uint8_t value;
	//value = ROBO3_ModuleProtocol::receiveData_byte(EX_DIGITAL_ID, Pin, _port);
	value = ROBO3_ModuleProtocol::receiveData_byte(0xfd, Pin, _port);
	return value;
}

uint8_t ROBO3_Extern::digitalWrite(uint8_t Pin,uint8_t state)
{
	uint8_t data[3]={EX_DIGITAL_ID,Pin, state};
	ROBO3_ModuleProtocol::sendData(0xfd, data, 3, _port);
}

uint16_t ROBO3_Extern::analogRead(uint8_t Pin)
{
	uint16_t value;
	Pin = (Pin+1)*2;
	//value = ROBO3_ModuleProtocol::receiveData_int(EX_ANALOG_ID, Pin, _port);
	value = ROBO3_ModuleProtocol::receiveData_int(0xfd, Pin, _port);
	return value;
}

void ROBO3_Extern::analogWrite(uint8_t Pin , uint8_t pwm)
{	
	uint8_t data[3]={EX_ANALOG_ID,Pin,pwm};
	// int16_t value1;
	//value1 = map(pwm,0,255,0,2000);
	// data[0]=Pin;
	// data[1]=pwm;//(uint8_t)(value1>>8);
	//data[2]=(uint8_t)value1;
	ROBO3_ModuleProtocol::sendData(0xfd, data, 3, _port);

}




