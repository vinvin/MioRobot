#include "ROBO3_Encodemotor.h"
#include "ROBO3_Config.h"

ROBO3_Encodemotor::ROBO3_Encodemotor(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_Encodemotor::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	
	_port = port;
	
	// if(port == MIO_M1)
	// {
		// _INA_pin = M1_INA_PIN;
		// _INB_pin = M1_INB_PIN;
	// }
	// else if(port == MIO_M2)    //port == M2
	// {
		// _INA_pin = M2_INA_PIN;
		// _INB_pin = M2_INB_PIN;
	// }
	// else
	// {
	// }
	 ROBO3_ModuleProtocol::init();

}

void ROBO3_Encodemotor::run_pwm(int16_t pwm1,int16_t pwm2)
{
	uint8_t V_speed[5]={0};

	V_speed[0] = 1;
	V_speed[1] = (uint8_t)(pwm1>>8);
	V_speed[2] = (uint8_t)pwm1;
	V_speed[3] = (uint8_t)(pwm2>>8);
	V_speed[4] = (uint8_t)pwm2;


	ROBO3_ModuleProtocol::sendData(ENCODEMOTOR_ID, V_speed, 5, _port);
}

void ROBO3_Encodemotor::run_speed(int16_t speed1,int16_t speed2)
{
	uint8_t data[5]={0};
	data[0] = 2;
	data[1] = (uint8_t)(speed1>>8);
	data[2] = (uint8_t)speed1;
	data[3] = (uint8_t)(speed2>>8);
	data[4] = (uint8_t)speed2;

	ROBO3_ModuleProtocol::sendData(ENCODEMOTOR_ID, data, 5, _port);
}
void ROBO3_Encodemotor::run_angle(int16_t angle1, int16_t speed1,int16_t angle2,int16_t speed2)
{
	uint8_t data[9]={0};
	data[0] = 3;
	data[1] = (uint8_t)(angle1>>8);
	data[2] = (uint8_t)angle1;
	data[3] = (uint8_t)(speed1>>8);
	data[4] = (uint8_t)speed1;
	data[5] = (uint8_t)(angle2>>8);
	data[6] = (uint8_t)angle2;
	data[7] = (uint8_t)(speed2>>8);
	data[8] = (uint8_t)speed2;

	ROBO3_ModuleProtocol::sendData(ENCODEMOTOR_ID, data, 9, _port);
	
}

int16_t ROBO3_Encodemotor::read_speed(uint8_t motor)
{
	int16_t motor_speed= 0;
	uint8_t motor_No;
	motor_No = (motor *2)-1;
		motor_speed = ROBO3_ModuleProtocol::receiveData_int(ENCODEMOTOR_ID, motor_No, _port); 
	return motor_speed;
}