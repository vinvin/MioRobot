#ifndef ROBO3_TM1650_H
#define ROBO3_TM1650_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_SoftIIC.h"
#include "ROBO3_Config.h"

/* Accessing a LED driver over I2C */

class ROBO3_TM1650 : public ROBO3_SoftIIC
{
	public:
		ROBO3_TM1650();
		void init_SoftIIC(uint8_t sclPin, uint8_t sdaPin);
		uint8_t getKeyboard();

	private:


};

#endif
