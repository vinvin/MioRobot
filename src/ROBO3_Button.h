#ifndef ROBO3_BUTTON_H
#define ROBO3_BUTTON_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

/* This exposes the programmable button labelled 'Key' at the back of MIO */

class ROBO3_Button : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Button(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint8_t read(void);
	private:
		uint8_t _KeyPin;
		uint8_t _port;
};

#endif 
