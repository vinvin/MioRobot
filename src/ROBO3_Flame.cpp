#include "ROBO3_Flame.h"
//#include <Wire.h>

ROBO3_Flame::ROBO3_Flame(void)
{
	//sensor.init();
	ROBO3_ModuleProtocol::init();
}

void ROBO3_Flame::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	
	ROBO3_ModuleProtocol::init();
}

uint16_t ROBO3_Flame::read(uint8_t sensors)
{
	uint16_t value;
	if(sensors==0)			
		value = ROBO3_ModuleProtocol::receiveData_byte(FLAME_ID, sensors, _port);
	else 
	{
		if(sensors>0&&sensors<6)
			sensors= sensors*2-1;
		else if(sensors<15)
			sensors= sensors+5;
		else 
			sensors=19;	
	value = ROBO3_ModuleProtocol::receiveData_int(FLAME_ID, sensors, _port);
    }
    return value;
}
uint16_t ROBO3_Flame::read_old(uint8_t sensors)
{
	uint16_t value=0;
	value = ROBO3_ModuleProtocol::receiveData_int(FLAME_ID, sensors, _port);
    
    return value;
}
void ROBO3_Flame::set_flame_value(uint8_t address,uint16_t data)
{	

    uint8_t data_buf[3] = {0};
	data = data;
	data_buf[0]=(address|0x10);
	data_buf[1]=(uint8_t)(data>>8);
	data_buf[2]=(uint8_t)data;
	sendData(FLAME_ID,data_buf,3,_port);
	
	// uint8_t data_iic[3] = {0};
	// data_iic[0] = address;
	// data_iic[1] = 0X00;	
	// data_iic[2] = 0X00;//Serial.println((data_iic[1]<<8)+data_iic[2]);
	// ROBO3_ModuleProtocol::sendData(FLAME_ID,data_iic,3,_port);
	// delay(5);
	// data_iic[1] = (uint8_t)(data>>8);	
	// data_iic[2] = (uint8_t)(data);//Serial.println((data_iic[1]<<8)+data_iic[2]);
	// delay(5);
	// ROBO3_ModuleProtocol::sendData(FLAME_ID,data_iic,3,_port);
}