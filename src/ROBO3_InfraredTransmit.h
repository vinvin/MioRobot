#ifndef ROBO3_INFRAREDTRANSMIT_H
#define ROBO3_INFRAREDTRANSMIT_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_MioBoard.h"

// emitting IR
class ROBO3_InfraredTransmit : public ROBO3_MioBoard
{
	public:
		ROBO3_InfraredTransmit();
		void setpin_mio(uint8_t port);
		void setpin_arduino(uint8_t pin);
		void turnOn();
		void turnOff();
		void write(uint8_t state);
	private:
		uint8_t _port;
		uint8_t _itPin;
};

#endif
