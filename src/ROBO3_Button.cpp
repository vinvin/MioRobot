#include "ROBO3_Button.h"

ROBO3_Button::ROBO3_Button(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_Button::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_KeyPin = pin;
	pinMode(_KeyPin,INPUT);
}

void ROBO3_Button::setpin_mio(uint8_t port)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		_KeyPin = Mio_Button_Pin;
		pinMode(_KeyPin,INPUT);
	}
	else
	{
		ROBO3_ModuleProtocol::init();
	}
}

uint8_t ROBO3_Button::read(void)
{
	uint8_t value;
	if(_port == 0)
		value = digitalRead(_KeyPin);
	else
	{
		value = ROBO3_ModuleProtocol::receiveData_byte(BUTTON_ID, 0x00, _port);
	}
	return value;
}

