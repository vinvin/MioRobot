#ifndef ROBO3_FLAME_H
#define ROBO3_FLAME_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

#define ALL_SENSORS   0
#define LEFT_SENSOR   1
#define MEDIAN_SENSOR 2
#define RIGHT_SENSOR  3
#define LEFTLEFT_SENSOR  4
#define RIGHTRIGHT_SENSOR  5

#define LEFT_ADDRSS  4
#define MEDIAN_ADDRSS  6
#define RIGHT_ADDRSS  8

/* Accessing the additional 5-angle flame detector
 * http://miorobot.es/formacion/sensor-de-llama
 */

class ROBO3_Flame : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Flame(void);
		void setpin_mio(uint8_t port);
		uint16_t read(uint8_t sensors);
		uint16_t read_old(uint8_t sensors);
		void set_flame_value(uint8_t address,uint16_t data);
	private:
		uint8_t  _port;
		//ROBO3_ModuleProtocol sensor;
};

#endif
