#include "ROBO3_Potentiometer.h"

ROBO3_Potentiometer::ROBO3_Potentiometer(void)
{
	ROBO3_ModuleProtocol::init();
}
void ROBO3_Potentiometer::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	PotentiometerValue_last = 0;
	ROBO3_ModuleProtocol::init();
	
}

uint16_t ROBO3_Potentiometer::read(void)
{
	uint8_t iicData[4],i=0;
	uint16_t value;
	value = ROBO3_ModuleProtocol::receiveData_int(POENTIOMETER_ID, 0x00, _port);
	//return value;
	
	if((value > 1024)||(value < 0))
	{
		tOut++;
		if((PotentiometerValue_last <= 1024)&&(PotentiometerValue_last >= 0))
		{
			if(tOut < 10)
				return(PotentiometerValue_last);
			else
			{
				tOut = 20;
				return(1024);
			}
		}
		else
			return(1024);
	}
	else
	{
		tOut = 0;
		PotentiometerValue_last = value;
		return value;
	}
}