#include "ROBO3_Key.h"

ROBO3_Key::ROBO3_Key(void)
{
	ROBO3_MioBoard::init();
}


void ROBO3_Key::setpin_mio(uint8_t port)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	ROBO3_ModuleProtocol::init();

}

uint8_t ROBO3_Key::read(uint8_t key_number)
{
	uint8_t value;
	if(_port == 0)
	{
		
	}
	else
	{
		value = ROBO3_ModuleProtocol::receiveData_byte(KEY_ID, key_number, _port);
	}
	return value;
}

