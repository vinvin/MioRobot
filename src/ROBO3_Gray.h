#ifndef ROBO3_GRAY_H
#define ROBO3_GRAY_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

/* Accessing the additional brightness level, or gray level, detector
 * http://miorobot.es/formacion/sensor-de-escala-de-grises
 */

class ROBO3_Gray : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Gray(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint16_t read();

	private:
		uint8_t _sensorPin;
		uint8_t _port;
		uint8_t count;
		uint16_t value;
		uint16_t value_old;
};

#endif
