#include "ROBO3_MioBoard.h"
#include <EEPROM.h>

ROBO3_MioBoard::ROBO3_MioBoard(void)
{
}

void ROBO3_MioBoard::init()
{
	//uint16_t version = ROBO3_MioBoard::EEPROMread16bit(HARDWARE_VERSION_ADDR);
	_version = ROBO3_MioBoard::EEPROMread16bit(HARDWARE_VERSION_ADDR);
	if(_version < 2000)//version 1
	{
		Mio_IR_Pin = 2;
		Mio_IT_Pin = 3;
		Mio_OTPdat_Pin = 4;
		M2_INA_PIN = 5;
		Mio_Servo_Pin1 = 6;
		MioBoardIO[2] = 7;
		MioBoardIO[1] = 8;
		M1_INB_PIN = 9;
		M1_INA_PIN = 10;
		M2_INB_PIN = 11;
		Mio_Servo_Pin2 = 12;
		Mio_RGBled_Pin = 13;
		MioBoardIO[3] = A0;
		MioBoardIO[4] = A1;
		Mio_SoundSensor_Pin = A2;
		Mio_OTPrst_Pin = A3;
		Mio_Button_Pin = A3;
		Mio_LightSensor_Pin = A6;	
		MioBoardIO[0] = 255;
	}
	else
	{
		Mio_IR_Pin = 2;
		M1_INB_PIN = 3;
		Mio_OTPdat_Pin = 4;
		M2_INB_PIN = 5;
		M2_INA_PIN = 6;
		MioBoardIO[2] = 7;
		MioBoardIO[1] = 8;
		Mio_Servo_Pin1 = 9;
		Mio_Servo_Pin2 = 10;
		M1_INA_PIN = 11; 
		Mio_IT_Pin = 12;
		Mio_RGBled_Pin = 13;
		MioBoardIO[3] = A0;
		MioBoardIO[4] = A1;
		Mio_SoundSensor_Pin = A2;
		Mio_OTPrst_Pin = A3;
		Mio_Button_Pin = A3;
		Mio_LightSensor_Pin = A6;	
		MioBoardIO[0] = 255;
	}
}

uint16_t ROBO3_MioBoard::version()
{
	return _version;
}
int16_t ROBO3_MioBoard::EEPROMread16bit(uint8_t addr)
{
    return (EEPROM.read(addr+1)<<8)|EEPROM.read(addr);
}

void ROBO3_MioBoard::EEPROMwrite16bit(uint8_t addr, uint16_t val)
{
    EEPROM.write(addr,(uint8_t)val);
    EEPROM.write(addr+1,(uint8_t)(val>>8));
}