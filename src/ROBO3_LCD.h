#ifndef ROBO3_LCD_H
#define ROBO3_LCD_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"
#include <Wire.h>

/* Accessing the additional LCD display
 * http://miorobot.es/formacion/pantalla-lcd
 */

class ROBO3_LCD : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_LCD();
		void setpin_mio(uint8_t port);
		void LCD_CLR(uint8_t color);	
		void LCD_SD(uint8_t picture_ID,uint16_t x,uint16_t y,uint8_t n);
		void LCD_HV(bool direction);
		void LCD_BL(uint8_t light);
		void LCD_PS(uint16_t x,uint16_t y,uint8_t color);
		void LCD_PL(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1,uint8_t color);
		void LCD_BOX(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1,uint8_t color,bool F);
		void LCD_CIR(uint16_t x,uint16_t y,uint16_t r,uint8_t color,bool F);
		void LCD_SBC(uint8_t color);
		void LCD_NUM(uint8_t number);
		void LCD_DCV(uint8_t size,uint16_t x,uint16_t y,bool F,uint8_t color,char* aaaa);
		uint8_t Check_sum(uint8_t* data,uint8_t n,bool sw);
		void I2C5_MasterSend(uint8_t* buffer,uint8_t len);
	private:
		uint8_t _port;
		uint8_t lcd_pin;
};

#endif
