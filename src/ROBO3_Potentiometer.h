#ifndef ROBO3_POENTIOMETER_H
#define ROBO3_POENTIOMETER_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

#define POENTIOMETER_ID  0x04

#define Mio_Potentiometer_Pin  0xff

/* Accessing the additional potentiometer module */

class ROBO3_Potentiometer : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Potentiometer();
		void setpin_mio(uint8_t port);
		uint16_t read();
	private:
		uint8_t _potentiometerr_pin;
		uint8_t _port;
		uint16_t PotentiometerValue_last;
		uint8_t tOut;
};

#endif
