#include "ROBO3_LightSensor.h"

ROBO3_LightSensor::ROBO3_LightSensor(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_LightSensor::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_sensorPin = pin;
	//ROBO3_ModuleProtocol::init(A5,A4,50);
}

void ROBO3_LightSensor::setpin_mio(uint8_t port)
{
	if(port > 4) port = 4;
	_port = port;

	if(_port == onBoardPort)
	{
		_sensorPin = Mio_LightSensor_Pin;
	}
	else
	{
		ROBO3_ModuleProtocol::init();
	}
}

uint16_t ROBO3_LightSensor::read(void)
{
	uint8_t iicData[2],i=0;
	uint16_t value;
	if(_port == 0)
		value = analogRead(_sensorPin);
	else
	{
		value = ROBO3_ModuleProtocol::receiveData_int(0xfe, LIGHTSENSOR_ID, _port);
	}
	return value;
}
