#ifndef ROBO3_SOFTIIC_H
#define ROBO3_SOFTIIC_H

#include <Arduino.h>

class ROBO3_SoftIIC
{
	public:
		ROBO3_SoftIIC();
		void masterInit(uint8_t sclPin, uint8_t sdaPin);
		int write(uint8_t sla_address, uint8_t sub_address, uint8_t data_w);
		uint8_t read(uint8_t sla_address, uint8_t sub_address);

		int start(void);
		void stop(void);
		void ack(void);
		void noAck(void);
		int waitAck(void);
		void sendByte(uint8_t SendByte);
		uint8_t receiveByte(void);

	private:
		void IICdelay(void);

		uint8_t SCL_PIN;
		uint8_t SDA_PIN;

};

#endif
