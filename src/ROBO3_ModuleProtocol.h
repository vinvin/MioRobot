#ifndef ROBO3_MODULEPROTOCOL_H
#define ROBO3_MODULEPROTOCOL_H

#include <Arduino.h>
#include <Wire.h>
#include "ROBO3_MioBoard.h"

#define IO_PIN_IDLE    HIGH
#define IO_PIN_BUSY    LOW

#define READ_MODULE  0
#define WRITE_MODULE 1

/* Main class for communicating with MIO modules (used by all others) */
class ROBO3_ModuleProtocol : public ROBO3_MioBoard
{
	public:
		ROBO3_ModuleProtocol();
		void init();
		void sendData(uint8_t addr, uint8_t* value, uint8_t length, uint8_t IOport);
		void sendData_for_lcd(uint8_t addr, uint8_t* value, uint8_t length, uint8_t CHECK, uint8_t IOport);
		// void sendData_for_c_lcd(uint8_t addr, uint8_t* value, uint8_t length, uint8_t IOport);
		// uint32_t receiveData_long(uint8_t addr, uint8_t cmd, uint8_t IOport);
		uint16_t receiveData_int(uint8_t addr, uint8_t cmd, uint8_t IOport);
		uint8_t receiveData_byte(uint8_t addr, uint8_t cmd, uint8_t IOport);
		short receiveData_short(uint8_t addr, uint8_t cmd, uint8_t IOport);
		void receiveData_buffer(uint8_t addr, uint8_t cmd, uint8_t *data_buffer,int length, uint8_t IOport); //length 个字节
	private:
		// uint32_t _period;
		// uint8_t _sclPin;
		// uint8_t _sdaPin;
		// void sendStart(uint8_t IOpin);
		// void sendEnd(uint8_t IOpin);
		// void sendByte(uint8_t value);
		// void sendByte_for_lcd(uint8_t value);
		uint8_t IO_pin[5];
		//Wire IIC;
};

#endif
