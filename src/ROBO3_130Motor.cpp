#include "ROBO3_130Motor.h"

ROBO3_130Motor::ROBO3_130Motor(void)
{
	ROBO3_MioBoard::init();
	ROBO3_ModuleProtocol::init();
}

void ROBO3_130Motor::setpin_arduino(uint8_t pin)
{
	_port = 0;
	pinMode(pin, OUTPUT);
}

void ROBO3_130Motor::setpin_mio(uint8_t port, uint8_t num)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(num < 1) num = 1;
	if(num > 3) num = 3;
	//_num = num;
	
	if(_port == 0)
	{
		if(num == 1)
			Motor130_Pin = Mio_Servo_Pin1;
		else
			Motor130_Pin = Mio_Servo_Pin2;
		
		pinMode(Motor130_Pin, OUTPUT);
	}
	else
	{
		Motor130_Group[1]=num;
	}
}

void ROBO3_130Motor::run(uint8_t run_Speed)
{
	if(_port == 0)
	{
		digitalWrite(Motor130_Pin,run_Speed);
	}
	else
	{	if(run_Speed<2)
			Motor130_Group[0]=EX_DIGITAL_ID;
		else 
			Motor130_Group[0]=EX_ANALOG_ID;
		Motor130_Group[2]=run_Speed;
		ROBO3_ModuleProtocol::sendData(0xfd, Motor130_Group, 3, _port);
	}
}

