#include "ROBO3_LCD.h"
//#include <Wire.h>

ROBO3_LCD::ROBO3_LCD(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_LCD::setpin_mio(uint8_t port)
{	
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
}
uint8_t ROBO3_LCD::Check_sum(uint8_t* data,uint8_t n,bool sw)
{
	uint8_t i,sum = 0;
	if(sw)
	{
		for(i = 0;i < n;i++)
			sum+=data[i+7];
	}
	else
	{
		for(i = 0;i < n;i++)
			sum+=data[i];
	}
	return sum;
}
void ROBO3_LCD::I2C5_MasterSend(uint8_t* buffer,uint8_t len)
{
	uint8_t check=0;
	if(buffer[7]>0x0a)
	 {
		ROBO3_LCD::LCD_NUM(buffer[2]-11);
		buffer[7]/=8;
		buffer[7]+=9;
		if(buffer[7]>14)buffer[7]=14;
		delay(10);
	}
	check = Check_sum(buffer,len,1);
	ROBO3_ModuleProtocol::sendData_for_lcd(LCD_ID, buffer, len, check, _port);
}
void ROBO3_LCD::LCD_CLR(uint8_t color)
{
	uint8_t data[3]={0};
	data[0] = 0x01;
	data[1] = color;
	data[2] = Check_sum(data,2,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 3, _port);
}
void ROBO3_LCD::LCD_SD(uint8_t picture_ID,uint16_t x,uint16_t y,uint8_t n)
{
	uint8_t data[8]={0};
	data[0] = 0x02;
	data[1] = picture_ID;
	data[2] = (uint8_t)(x);
	data[3] = (uint8_t)(x>>8);
	data[4] = (uint8_t)(y);
	data[5] = (uint8_t)(y>>8);
	data[6] = (uint8_t)(n);
	data[7] = Check_sum(data,7,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 8, _port);
}
void ROBO3_LCD::LCD_HV(bool direction)
{
	uint8_t data[3]={0};
	data[0] = 0x03;
	data[1] = (uint8_t)direction;
	data[2] = Check_sum(data,2,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 3, _port);
}
void ROBO3_LCD::LCD_BL(uint8_t light)
{
	uint8_t data[3]={0};
	data[0] = 0x04;
	data[1] = 255-light;
	data[2] = Check_sum(data,2,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 3, _port);
}
void ROBO3_LCD::LCD_PS(uint16_t x,uint16_t y,uint8_t color)
{
	uint8_t data[8]={0};
	data[0] = 0x05;
	data[1] = (uint8_t)(x);
	data[2] = (uint8_t)(x>>8);
	data[3] = (uint8_t)(y);
	data[4] = (uint8_t)(y>>8);
	data[5] = (uint8_t)(color);
	data[6] = Check_sum(data,6,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 8, _port);
}
void ROBO3_LCD::LCD_PL(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1,uint8_t color)
{
	uint8_t data[11]={0};
	data[0] = 0x06;
	data[1] = (uint8_t)(x0);
	data[2] = (uint8_t)(x0>>8);
	data[3] = (uint8_t)(y0);
	data[4] = (uint8_t)(y0>>8);
	data[5] = (uint8_t)(x1);
	data[6] = (uint8_t)(x1>>8);
	data[7] = (uint8_t)(y1);
	data[8] = (uint8_t)(y1>>8);
	data[9] = (uint8_t)(color);
	data[10] = Check_sum(data,10,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 11, _port);
}
void ROBO3_LCD::LCD_BOX(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1,uint8_t color,bool F)
{
	uint8_t data[12]={0};
	data[0] = 0x07;
	data[1] = (uint8_t)(x0);
	data[2] = (uint8_t)(x0>>8);
	data[3] = (uint8_t)(y0);
	data[4] = (uint8_t)(y0>>8);
	data[5] = (uint8_t)(x1);
	data[6] = (uint8_t)(x1>>8);
	data[7] = (uint8_t)(y1);
	data[8] = (uint8_t)(y1>>8);
	data[9] = (uint8_t)(color);
	data[10] = (uint8_t)(F);
	data[11] = Check_sum(data,11,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 12, _port);
}
void ROBO3_LCD::LCD_CIR(uint16_t x,uint16_t y,uint16_t r,uint8_t color,bool F)
{
	uint8_t data[10]={0};
	data[0] = 0x08;
	data[1] = (uint8_t)(x);
	data[2] = (uint8_t)(x>>8);
	data[3] = (uint8_t)(y);
	data[4] = (uint8_t)(y>>8);
	data[5] = (uint8_t)(r);
	data[6] = (uint8_t)(r>>8);
	data[7] = (uint8_t)(color);
	data[8] = (uint8_t)(F);
	data[9] = Check_sum(data,9,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 10, _port);
}
void ROBO3_LCD::LCD_SBC(uint8_t color)
{
	uint8_t data[3]={0};
	data[0] = 0x09;
	data[1] = (uint8_t)color;
	data[2] = Check_sum(data,2,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data, 3, _port);
}
void ROBO3_LCD::LCD_NUM(uint8_t number)
{
	uint8_t data[4]={0};
	data[0] = 0x0A;
	data[1] = (uint8_t)(number);
	data[2] = (uint8_t)(number>>8);
	data[3] = Check_sum(data,3,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data,4, _port);
}
void ROBO3_LCD::LCD_DCV(uint8_t size,uint16_t x,uint16_t y,bool F,uint8_t color,char* aaaa)
{
	uint8_t data[20]={0};
	uint8_t temp = 0,i=0;
	temp = strlen(aaaa);
	ROBO3_LCD::LCD_NUM(temp);
  delayMicroseconds(500);
	switch(size)
	{
		case 16:data[0] = 0x0B;break;
		case 24:data[0] = 0x0C;break;
		case 32:data[0] = 0x0D;break;
		case 48:data[0] = 0x0E;break;
	}
	data[1] = (uint8_t)(x);
	data[2] = (uint8_t)(x>>8);
	data[3] = (uint8_t)(y);
	data[4] = (uint8_t)(y>>8);
	data[5] = (uint8_t)(F);
	data[6] = (uint8_t)color;
	for(i=0;i<temp;i++)
	{
		data[i+7] = aaaa[i];
	}
	data[7+temp] = Check_sum(data,7+temp,0);
	ROBO3_ModuleProtocol::sendData(LCD_ID, data,8+temp, _port);
}

// void ROBO3_LCD::LCD_DCV24(uint16_t x,uint16_t y,bool F,uint8_t color,char* aaaa)
// {
	// uint8_t data[20]={0};
	// uint8_t temp = 0,i=0;
	// temp = strlen(aaaa);
	// data[0] = 0x0c;
	// data[1] = (uint8_t)(x);
	// data[2] = (uint8_t)(x>>8);
	// data[3] = (uint8_t)(y);
	// data[4] = (uint8_t)(y>>8);
	// data[5] = (uint8_t)(F);
	// data[6] = (uint8_t)color;
	// for(i=0;i<temp;i++)
	// {
		// data[i+7] = aaaa[i];
	// }
	// data[7+temp] = Check_sum(data,7+temp,0);
	// ROBO3_ModuleProtocol::sendData(LCD_ID, data,8+temp, _port);
	
// }
// void ROBO3_LCD::LCD_DCV32(uint16_t x,uint16_t y,bool F,uint8_t color,char* aaaa)
// {
	// uint8_t data[20]={0};
	// uint8_t temp = 0,i=0;
	// temp = strlen(aaaa);
	// data[0] = 0x0d;
	// data[1] = (uint8_t)(x);
	// data[2] = (uint8_t)(x>>8);
	// data[3] = (uint8_t)(y);
	// data[4] = (uint8_t)(y>>8);
	// data[5] = (uint8_t)(F);
	// data[6] = (uint8_t)color;
	// for(i=0;i<temp;i++)
	// {
		// data[i+7] = aaaa[i];
	// }
	// data[7+temp] = Check_sum(data,7+temp,0);
	// ROBO3_ModuleProtocol::sendData(LCD_ID, data,8+temp, _port);
// }
// void ROBO3_LCD::LCD_DCV48(uint16_t x,uint16_t y,bool F,uint8_t color,char* aaaa)
// {
	// uint8_t data[20]={0};
	// uint8_t temp = 0,i=0;
	// temp = strlen(aaaa);
	// data[0] = 0x0E;
	// data[1] = (uint8_t)(x);
	// data[2] = (uint8_t)(x>>8);
	// data[3] = (uint8_t)(y);
	// data[4] = (uint8_t)(y>>8);
	// data[5] = (uint8_t)(F);
	// data[6] = (uint8_t)color;
	// for(i=0;i<temp;i++)
	// {
		// data[i+7] = aaaa[i];
	// }
	// data[7+temp] = Check_sum(data,7+temp,0);
	// ROBO3_ModuleProtocol::sendData(LCD_ID, data,8+temp, _port);
// }






