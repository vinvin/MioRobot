#include "ROBO3_InfraredTransmit.h"

ROBO3_InfraredTransmit::ROBO3_InfraredTransmit(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_InfraredTransmit::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_itPin = pin;
	pinMode(_itPin,OUTPUT);
	digitalWrite(_itPin, LOW);
}

void ROBO3_InfraredTransmit::setpin_mio(uint8_t port)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		_itPin = Mio_IT_Pin;
		pinMode(_itPin,OUTPUT);
		digitalWrite(_itPin, LOW);
	}
}

void ROBO3_InfraredTransmit::turnOn()
{
	if(_port == 0)
	{
		digitalWrite(_itPin, HIGH);
	}
	else
	{
		
	}
}

void ROBO3_InfraredTransmit::turnOff()
{
	if(_port == 0)
	{
		digitalWrite(_itPin, LOW);
	}
	else
	{
		
	}
}

void ROBO3_InfraredTransmit::write(uint8_t state)
{
	if(state == 0)
	{
		ROBO3_InfraredTransmit::turnOff();
	}
	else
	{
		ROBO3_InfraredTransmit::turnOn();
	}
}