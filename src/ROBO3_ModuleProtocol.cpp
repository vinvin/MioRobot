#include "ROBO3_ModuleProtocol.h"
#include <Arduino.h>
#include <Wire.h>

ROBO3_ModuleProtocol::ROBO3_ModuleProtocol()
{
	ROBO3_MioBoard::init();
}

// void ROBO3_ModuleProtocol::init(uint8_t SCLpin, uint8_t SDApin, uint32_t period)
void ROBO3_ModuleProtocol::init()
{
	Wire.begin(); // join i2c bus (address optional for master)
	Wire.setClock(400000);

	for(uint8_t i=1;i<5;i++)
	{
		IO_pin[i] = MioBoardIO[i];
		pinMode(IO_pin[i],OUTPUT);
		digitalWrite(IO_pin[i],HIGH);
	}
}
/*************************************  2018-06-25 By gcy start*************************************/

void ROBO3_ModuleProtocol::sendData(uint8_t addr, uint8_t* value, uint8_t length, uint8_t IOport)
{
	// static uint8_t port = IO_pin[IOport];

	//uint8_t port = IO_pin[IOport];
	digitalWrite(IO_pin[IOport],IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr);
	for(uint8_t i=0;i<length;i++)
	{
		Wire.write(value[i]);
	}
	Wire.endTransmission(); // 停止发送
	digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
}
void ROBO3_ModuleProtocol::sendData_for_lcd(uint8_t addr, uint8_t* value, uint8_t length, uint8_t CHECK, uint8_t IOport)
{
	// static uint8_t port = IO_pin[IOport];

	//uint8_t port = IO_pin[IOport];
	digitalWrite(IO_pin[IOport],IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr);
	for(uint8_t i=0;i<length;i++)
	{
		Wire.write(value[i+7]);
	}
	Wire.write(CHECK);
	Wire.endTransmission(); // 停止发送
	digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
}
short ROBO3_ModuleProtocol::receiveData_short(uint8_t addr, uint8_t cmd, uint8_t IOport)
{
	// static short return_vlaue = 0;
	// static uint8_t return_vlaue_buffer[12] = {0};
	// static uint8_t return_vlaue_number = 0;
	// static uint8_t port = IO_pin[IOport];

	short return_vlaue = 0;
	uint8_t return_vlaue_buffer[12] = {0};
	uint8_t return_vlaue_number = 0;
	//uint8_t port = IO_pin[IOport];
	digitalWrite(IO_pin[IOport],IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr); 
	Wire.write(cmd);
	Wire.endTransmission(); // 停止发送
	delayMicroseconds(100);
	Wire.requestFrom((int)addr,2,1); 
	while(Wire.available()) 
	{ 
		return_vlaue_buffer[return_vlaue_number++] = Wire.read();
	} 
	return_vlaue = return_vlaue_buffer[0]+ (return_vlaue_buffer[1]*256);

	digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
	return return_vlaue;
}
uint16_t ROBO3_ModuleProtocol::receiveData_int(uint8_t addr, uint8_t cmd, uint8_t IOport)
{
	// static uint16_t return_vlaue = 0;
	// static uint8_t return_vlaue_buffer[12] = {0};
	// static uint8_t return_vlaue_number = 0;
	// static uint8_t port = IO_pin[IOport];

	uint16_t return_vlaue = 0;
	uint8_t return_vlaue_buffer[12] = {0};
	uint8_t return_vlaue_number = 0;
	//uint8_t port = IO_pin[IOport];
	digitalWrite(IO_pin[IOport],IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr); 
	Wire.write(cmd);
	Wire.endTransmission(); // 停止发送
	delayMicroseconds(100);
	Wire.requestFrom((int)addr,2,1); 
	while(Wire.available()) 
	{ 
		return_vlaue_buffer[return_vlaue_number++] = Wire.read();
	} 
	return_vlaue = return_vlaue_buffer[0]+ (return_vlaue_buffer[1]*256);

	digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
	return return_vlaue;
}
uint8_t ROBO3_ModuleProtocol::receiveData_byte(uint8_t addr, uint8_t cmd, uint8_t IOport)
{
	// static uint16_t return_vlaue = 0;
	// static uint8_t port = IO_pin[IOport];

	uint8_t return_vlaue = 0;
	//uint8_t port = IO_pin[IOport];
	digitalWrite(IO_pin[IOport],IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr); 
	Wire.write(cmd);
	Wire.endTransmission(); // 停止发送
	delayMicroseconds(100);
	Wire.requestFrom((int)addr,1,1); 
	while(Wire.available()) 
	{ 
		return_vlaue = Wire.read();
	}   
	digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
	return return_vlaue;
}

void ROBO3_ModuleProtocol::receiveData_buffer(uint8_t addr, uint8_t cmd, uint8_t *data_buffer,int length, uint8_t IOport) //length 个字节
{
	// static uint16_t return_vlaue = 0;
	// static uint8_t return_vlaue_buffer[12] = {0};
	// static uint8_t return_vlaue_number = 0;
	// static uint8_t port = IO_pin[IOport];

	uint16_t return_vlaue = 0;
	//uint8_t return_vlaue_buffer[length] = {0};
	uint8_t return_vlaue_number = 0;
	uint8_t port = IO_pin[IOport];
	digitalWrite(port,IO_PIN_BUSY);
	delayMicroseconds(100);
	Wire.beginTransmission(addr); 
	Wire.write(cmd);
	Wire.endTransmission(); 
	delayMicroseconds(100);
	Wire.requestFrom((int)addr,length,1); 
	while(Wire.available()) 
	{ 
		data_buffer[return_vlaue_number++] = Wire.read();
	}  
	delayMicroseconds(100);
	digitalWrite(port,IO_PIN_IDLE);
}
/*************************************  2018-06-25 By gcy end*************************************/

// void ROBO3_ModuleProtocol::sendData_for_lcd(uint8_t addr, uint8_t* value, uint8_t length, uint8_t CHECK, uint8_t IOport)
// {
// if(IOport<1) IOport = 1;
// if(IOport>4) IOport = 4;

// pinMode(IO_pin[IOport], OUTPUT);
// pinMode(_sclPin, OUTPUT);
// pinMode(_sdaPin, OUTPUT);

// digitalWrite(IO_pin[IOport],IO_PIN_BUSY);delay(1);
// digitalWrite(_sdaPin, HIGH);
// digitalWrite(_sclPin, HIGH);
// digitalWrite(_sdaPin, LOW);
// digitalWrite(_sclPin, LOW);

// delayMicroseconds(10);
// sendByte_for_lcd((addr<<1)|0);
// for(uint8_t i=0;i<length;i++)
// {
// sendByte_for_lcd(value[i+7]);
// }
// sendByte_for_lcd(CHECK);	
// digitalWrite(_sclPin, HIGH);
// digitalWrite(_sdaPin, HIGH);

// delay(1);
// digitalWrite(IO_pin[IOport],IO_PIN_IDLE);
// }
// void ROBO3_ModuleProtocol::sendData_for_c_lcd(uint8_t addr, uint8_t* value, uint8_t length, uint8_t IOport)
// {
// if(IOport<1) IOport = 1;
// if(IOport>4) IOport = 4;

// pinMode(IO_pin[IOport], OUTPUT);
// pinMode(_sclPin, OUTPUT);
// pinMode(_sdaPin, OUTPUT);

// digitalWrite(IO_pin[IOport],IO_PIN_BUSY);	
// delayMicroseconds(1000);
// digitalWrite(_sdaPin, HIGH);
// digitalWrite(_sclPin, HIGH);
// digitalWrite(_sdaPin, LOW);
// digitalWrite(_sclPin, LOW);

// delayMicroseconds(10);
// sendByte_for_lcd((addr<<1)|0);
// for(uint8_t i=0;i<length;i++)
// {
// sendByte_for_lcd(value[i]);
// }	
// digitalWrite(_sclPin, HIGH);
// digitalWrite(_sdaPin, HIGH);
// delayMicroseconds(1000);
// digitalWrite(IO_pin[IOport],IO_PIN_IDLE);

// }
// uint32_t ROBO3_ModuleProtocol::receiveData_long(uint8_t addr, uint8_t cmd, uint8_t IOport)
// {
// uint8_t i=0;
// uint64_t value=0;
// uint32_t timeOut = 0;

// if(IOport<1) IOport = 1;
// if(IOport>4) IOport = 4;
// pinMode(IO_pin[IOport], OUTPUT);
// pinMode(_sclPin, OUTPUT);
// pinMode(_sdaPin, OUTPUT);

// sendStart(IO_pin[IOport]);
// sendByte((addr<<1)|READ_MODULE);
// sendByte(cmd);
// sendEnd(IO_pin[IOport]);

// pinMode(IO_pin[IOport],INPUT_PULLUP);
// pinMode(_sclPin,INPUT);
// pinMode(_sdaPin,INPUT);

// while(digitalRead(IO_pin[IOport])==IO_PIN_IDLE)
// {
// uint8_t n=30;
// while(n--);
// timeOut++;
// if(timeOut>100000)
// break;
// }
// if(timeOut>100000)
// return 0;
// for(i=0;i<40;i++)
// {
// timeOut = 0;
// while(digitalRead(_sclPin)==LOW)
// {
// uint8_t n=30;
// while(n--);
// timeOut++;
// if(timeOut>20000)
// break;
// }
// if(digitalRead(IO_pin[IOport]) == IO_PIN_BUSY)
// value |= (uint32_t)digitalRead(_sdaPin)<<(uint32_t)i;
// else
// break;
// while(digitalRead(_sclPin)==HIGH)
// {
// uint8_t n=30;
// while(n--);
// timeOut++;
// if(timeOut>20000)
// break;
// }
// }
// if(i==40)
// return (uint32_t)((uint64_t)value>>(uint64_t)8);
// else
// return 0;
// }
