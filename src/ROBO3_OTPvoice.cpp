#include "ROBO3_OTPvoice.h"

ROBO3_OTPvoice::ROBO3_OTPvoice(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_OTPvoice::setpin_arduino(uint8_t resetPin, uint8_t dataPin)
{
	_port = 0;
	_DataPin = dataPin;
	_ResetPin = resetPin;
	pinMode(_ResetPin, OUTPUT);
	pinMode(_DataPin, OUTPUT);
}

void ROBO3_OTPvoice::setpin_mio(uint8_t port)
{	
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		_ResetPin = Mio_OTPrst_Pin;
		_DataPin = Mio_OTPdat_Pin;
		pinMode(_ResetPin, OUTPUT);
		pinMode(_DataPin, OUTPUT);
	}
	else
	{
	}
}

void ROBO3_OTPvoice::write(uint8_t part)
{
	if(_port == 0)
	{
		digitalWrite(_ResetPin, HIGH);
		delayMicroseconds(100);
		digitalWrite(_ResetPin, LOW);
		delayMicroseconds(100);
		while(part)
		{
			digitalWrite(_DataPin, HIGH);
			delayMicroseconds(100);
			digitalWrite(_DataPin, LOW);
			delayMicroseconds(100);
			part--;
		}
	}
	else
	{
	}
}

