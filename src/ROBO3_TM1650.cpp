/* Includes ------------------------------------------------------------------*/
#include "ROBO3_TM1650.h"
#include "ROBO3_SoftIIC.h"

ROBO3_TM1650::ROBO3_TM1650()
{
}

void ROBO3_TM1650::init_SoftIIC(uint8_t sclPin, uint8_t sdaPin)
{
	ROBO3_SoftIIC::masterInit(sclPin, sdaPin);
}

uint8_t ROBO3_TM1650::getKeyboard()
{
	uint8_t key;
	ROBO3_SoftIIC::start();
	ROBO3_SoftIIC::sendByte(0x49);
	ROBO3_SoftIIC::waitAck();
	key = ROBO3_SoftIIC::receiveByte();
	ROBO3_SoftIIC::waitAck();
	ROBO3_SoftIIC::stop();
	
	return key;
}