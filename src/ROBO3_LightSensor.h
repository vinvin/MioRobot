#ifndef ROBO3_LIGHTSENSOR_H
#define ROBO3_LIGHTSENSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

#define LIGHTSENSOR_ID      0x03

#define Mio_LightSensor_Pin A6

/* Accessing the onboard light sensor of MIO */

class ROBO3_LightSensor : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_LightSensor(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint16_t read(void);

	private:
		uint8_t _sensorPin;
		uint8_t _port;
};

#endif
