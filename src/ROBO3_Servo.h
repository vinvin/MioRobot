#ifndef ROBO3_SERVO_H
#define ROBO3_SERVO_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include <Servo.h>
#include "ROBO3_ModuleProtocol.h"

/* Accessing the servomotor ports of the MIO */

class ROBO3_Servo : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Servo(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port, uint8_t num);
		void runTo(uint8_t angle);
		void servorun(uint8_t servonum, uint8_t angle);
		void stop_Servo();
	private:
		uint8_t _servoPin;
		uint8_t _port;
		uint8_t _num;
		uint8_t servo_num;
		Servo sv;
};

#endif
