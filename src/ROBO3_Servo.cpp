#include "ROBO3_Servo.h"

ROBO3_Servo::ROBO3_Servo(void)
{
	ROBO3_MioBoard::init();
	ROBO3_ModuleProtocol::init();
}

void ROBO3_Servo::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_servoPin = pin;
	pinMode(_servoPin,OUTPUT);
	sv.attach(_servoPin);
}
void ROBO3_Servo::stop_Servo(void)
{	
	sv.detach();
}
void ROBO3_Servo::setpin_mio(uint8_t port, uint8_t num)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(num < 1) num = 1;
	_num = num;
	
	if(_port == 0)
	{
		if(num == 1)
			_servoPin = Mio_Servo_Pin1;
		else
			_servoPin = Mio_Servo_Pin2;
		if(ROBO3_MioBoard::version() < 2000)
		{
			pinMode(_servoPin,OUTPUT);
			digitalWrite(_servoPin,LOW);
		}
		else
		{
			sv.attach(_servoPin);
		}
	}
	else
	{
		servo_num = num;
		// if(num==1)servo_num=0x01;
		// else if(num==2)servo_num=0x02;
		// else if(num==3)servo_num=0x03;
		// else servo_num=0;
	}
}

void ROBO3_Servo::runTo(uint8_t angle)
{
	if(_port == 0)
	{
		if(ROBO3_MioBoard::version() < 2000)
		{
			int16_t value;
			value = map(angle,0,180,500,2500);
			digitalWrite(_servoPin, HIGH);
			delayMicroseconds(value);
			digitalWrite(_servoPin,LOW);
		}
		else
		{
			sv.write(angle);
		}
	}
	else
	{
		uint8_t data[4]={0};
        int16_t value1;
		value1 = map(angle,5,175,500,2500);
		data[0]=SERVO_ID;
		data[1]=servo_num;
		data[2]=(uint8_t)(value1>>8);
		data[3]=(uint8_t)value1;
		ROBO3_ModuleProtocol::sendData(0XFD, data, 4, _port);
	}
}