#ifndef ROBO3_EXTERN_H
#define ROBO3_EXTERN_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

/* Generic sensor read and write */

class ROBO3_Extern : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Extern(void);
		void setpin_mio(uint8_t port);
		uint8_t digitalRead(uint8_t Pin);
		uint8_t digitalWrite(uint8_t Pin,uint8_t state);
		uint16_t analogRead(uint8_t Pin);
		void analogWrite(uint8_t Pin, uint8_t pwm);
	private:
		uint8_t _port;
};

#endif
