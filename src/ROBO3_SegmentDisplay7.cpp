#include "ROBO3_SegmentDisplay7.h"
ROBO3_SegmentDisplay7::ROBO3_SegmentDisplay7(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_SegmentDisplay7::setpin_mio(uint8_t port)
{
	ROBO3_ModuleProtocol::init();
	
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
}

void ROBO3_SegmentDisplay7::showNumber(float number)
{
	//number = -12.1;
	uint8_t data[5]={0};
     val_7.floatVal = number;
		data[0] = 1;
	 data[1] = val_7.byteVal[0];
	 data[2] = val_7.byteVal[1];
	 data[3] = val_7.byteVal[2];
	 data[4] = val_7.byteVal[3];
	ROBO3_ModuleProtocol::sendData(SegmentDisplay7_ID, data, 5, _port);
}
