#include "ROBO3_MPU6050.h"

 //exuint16_t receiveValue[32]
ROBO3_MPU6050::ROBO3_MPU6050(void)
{
	//sensor.init();
	ROBO3_ModuleProtocol::init();
}

void ROBO3_MPU6050::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	
	ROBO3_ModuleProtocol::init();
}

short ROBO3_MPU6050::read(uint8_t sensors)
{
	short value = 0;
	sensors =((sensors-1)*2);
	value = ROBO3_ModuleProtocol::receiveData_short(MPU6050_ID, sensors, _port);//
    return value;     
}
void ROBO3_MPU6050::read_buffer(uint8_t *buffer)
{	
	ROBO3_ModuleProtocol::receiveData_buffer(MPU6050_ID, 0x06, buffer,12, _port); 
}






