#ifndef ROBO3_KEY_H
#define ROBO3_KEY_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

/* Accessing the additional 8-button module (I guess) */

class ROBO3_Key : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_Key(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint8_t read(uint8_t key_number);
	private:
		uint8_t _port;
};

#endif
