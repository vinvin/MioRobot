#ifndef ROBO3_COLORSENSOR_H
#define ROBO3_COLORSENSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

#define RED 0X01
#define GREEN 0X02
#define BLUE 0X03

/* Accessing the additional color sensor
 * http://miorobot.es/formacion/sensor-de-color */

class ROBO3_ColorSensor: public ROBO3_ModuleProtocol
{
	public:
		ROBO3_ColorSensor(void);
		void setpin_arduino(uint8_t pin);
		void setpin_mio(uint8_t port);
		uint16_t read(uint8_t color);

	private:
		uint8_t _sensorPin;
		uint8_t _port;
};

#endif
