#include "ROBO3_LineFollower.h"
//#include <Wire.h>

ROBO3_LineFollower::ROBO3_LineFollower(void)
{
	//sensor.init();
	ROBO3_ModuleProtocol::init();
}

void ROBO3_LineFollower::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	
	ROBO3_ModuleProtocol::init();
}

uint8_t ROBO3_LineFollower::read(uint8_t sensors)
{
	uint8_t value;
	
	//value = sensor.receiveData_byte(LINEFOLLOWER_ID, sensors, _port);
	value = ROBO3_ModuleProtocol::receiveData_byte(LINEFOLLOWER_ID, sensors, _port);
   
    return value;
}
uint16_t ROBO3_LineFollower::read_old(uint8_t sensors)
{
	uint16_t value=0;
	if(sensors>14)
		sensors=14;
	value = ROBO3_ModuleProtocol::receiveData_int(LINEFOLLOWER_ID, sensors, _port);
    
    return value;
}
void ROBO3_LineFollower::set_light_value(uint8_t address,uint16_t data)
{	
    uint8_t data_buf[3] = {0};
	data = data*4;
	data_buf[0]=(address|0x10);
	data_buf[1]=(uint8_t)(data>>8);
	data_buf[2]=(uint8_t)data;
	sendData(LINEFOLLOWER_ID,data_buf,3,_port);

	// uint8_t data_iic[3] = {0};
	// data_iic[0] = address;
	// data_iic[1] = 0X00;	
	// data_iic[2] = 0X00;Serial.println((data_iic[1]<<8)+data_iic[2]);
	// ROBO3_ModuleProtocol::sendData(LINEFOLLOWER_ID,data_iic,3,_port);
	// delay(5);
	// data*=4;
	// data_iic[1] = (uint8_t)(data>>8);	
	// data_iic[2] = (uint8_t)(data);Serial.println((data_iic[1]<<8)+data_iic[2]);
	// delay(5);
	// ROBO3_ModuleProtocol::sendData(LINEFOLLOWER_ID,data_iic,3,_port);
	//ROBO3_ModuleProtocol::sendData(LINEFOLLOWER_ID,data_iic,3,_port);
}