#include "ROBO3_ColorSensor.h"
ROBO3_ColorSensor::ROBO3_ColorSensor(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_ColorSensor::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_sensorPin = pin;
	//ROBO3_ModuleProtocol::init(A5,A4,50);
}

void ROBO3_ColorSensor::setpin_mio(uint8_t port)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		;//_sensorPin = Mio_LightSensor_Pin;
	}
	else
	{
		ROBO3_ModuleProtocol::init();	
	}
}

uint16_t ROBO3_ColorSensor::read(uint8_t color)
{
	//uint8_t iicData[2],i=0;
	uint16_t value;
	color*=2;
	if(_port == 0)
		;//value = analogRead(_sensorPin);
	// else if(_port==1|_port==2|_port==3|_port==4)
	// {
		// value = ROBO3_ModuleProtocol::receiveData_int(COLORSENSOR_ID, color, _port);
		// if(value == 0)
		// value = ROBO3_ModuleProtocol::receiveData_int(COLORSENSOR_ID, color, _port);
	// }
	else
	{
		value = ROBO3_ModuleProtocol::receiveData_int(COLORSENSOR_ID, color, _port);   
	}
	return value>>4;
}
