#include "ROBO3_DCMotorTC118S.h"
#include "ROBO3_Config.h"

ROBO3_DCMotorTC118S::ROBO3_DCMotorTC118S(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_DCMotorTC118S::setpin_arduino(uint8_t INAPin,uint8_t INBPin)
{
	_port = 0;
	_INA_pin = INAPin;
	_INB_pin = INBPin;
}

void ROBO3_DCMotorTC118S::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > MIO_M2) port = MIO_M2;
	
	_port = port;
	
	if(port == MIO_M1)
	{
		_INA_pin = M1_INA_PIN;
		_INB_pin = M1_INB_PIN;
	}
	else if(port == MIO_M2)    //port == M2
	{
		_INA_pin = M2_INA_PIN;
		_INB_pin = M2_INB_PIN;
	}
	else
	{
	}
	ROBO3_ModuleProtocol::init();

}

void ROBO3_DCMotorTC118S::run(int16_t speed)
{
	speed	= speed > 255 ? 255 : speed;
	speed	= speed < -255 ? -255 : speed;
	//speed = -speed;
	if((_port>=1)&&(_port<=4))
	{
	}
	else    // _port == 0, M1, M2
	{
		if(speed >= 0)
		{
			analogWrite(_INB_pin,255);
			analogWrite(_INA_pin,255-speed);
		}
		else
		{
			analogWrite(_INA_pin,255);
			analogWrite(_INB_pin,((uint8_t)speed)-1);
		}
	}
}

void ROBO3_DCMotorTC118S::run(int16_t speed1,int16_t speed2)
{
	uint8_t V_speed[5]={0};
	
		V_speed[0] = 1;
	if(speed1>=0)
		V_speed[1] = 1;
	else
		V_speed[1] = 0;
	V_speed[2] = abs(speed1);
	if(speed2>=0)
		V_speed[3] = 0;
	else
		V_speed[3] = 1;
	V_speed[4] = abs(speed2);	

	ROBO3_ModuleProtocol::sendData(DCMOTOR_ID, V_speed, 5, _port);
}