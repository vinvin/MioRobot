#include "ROBO3_UltrasonicSensor.h"
//#include <Wire.h>

ROBO3_UltrasonicSensor::ROBO3_UltrasonicSensor(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_UltrasonicSensor::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	//distanceLast = 0;
	
	ROBO3_ModuleProtocol::init();
	//pinMode(ROBO3_RJ25Port::getIOpin(_port), OUTPUT);
	//Wire.begin();
}

uint16_t ROBO3_UltrasonicSensor::read()
{
	// uint8_t iicData[4],i=0;
	// float distance;
	// uint16_t value;
	
	// /*****************20171023 by gcy ***********************/
	// static uint16_t valueLast;
	// if(millis()-busyTime>30)
	// {
		// busyTime = millis();
		// value = ROBO3_ModuleProtocol::receiveData_int(ULTRASONIC_ID, 0x00, _port);
		// valueLast = value;
	// }
	// else
	// {
		// value = valueLast;
	// }
	// /****************************************/	
	//value = ROBO3_ModuleProtocol::receiveData_int(ULTRASONIC_ID, 0x00, _port);
	// distance = value*34.0f/2000.0f;
	// return distance;
	
	uint8_t iicData[4],i=0;
	float distance;
	uint16_t value;
	value = ROBO3_ModuleProtocol::receiveData_int(ULTRASONIC_ID, 0x01, _port);
	if(value>32767)
		{			
		  distance= value-32767;
		}
	else
		distance = value*34.0/2000.0;
	
	if((distance > 300)||(distance < 1))
	{ 
     // delay(2);
	value = ROBO3_ModuleProtocol::receiveData_int(ULTRASONIC_ID, 0x01, _port);
	 	if(value>32767)
		{			
		  distance= value-32767;
		}
		else
			distance = value*34.0/2000.0;
	   
	if((distance > 300)||(distance < 1))
	{
	   delay(20);
	   tOut=2;
	   while(tOut--)
	   {
	value = ROBO3_ModuleProtocol::receiveData_int(ULTRASONIC_ID, 0x01, _port);
			if(value>32767)
			{			
		  distance= value-32767;
			}
			else
				distance = value*34.0/2000.0;
		   if((distance <= 300)&&(distance >= 1))
		{
			return (uint16_t)distance;
			
		}else if(tOut==1)
		{
			 distance = 300;
			 return (uint16_t)distance;
		}
	   }
	}

	}
	else
	{
		  delay(20);
		  return (uint16_t)distance;
	}
}