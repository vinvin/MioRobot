#ifndef ROBO3_PIR_H
#define ROBO3_PIR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

/* Accessing the additional PIR (infrared proximity) sensor module
 * http://miorobot.es/formacion/sensor-pir
 */

class ROBO3_PIR : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_PIR(void);
		void setpin_mio(uint8_t port);
		uint8_t read(void);
	private:
		uint8_t _KeyPin;
		uint8_t _port;
};

#endif
