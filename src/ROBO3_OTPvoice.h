#ifndef ROBO3_OTPVOICE_H
#define ROBO3_OTPVOICE_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_MioBoard.h"

#define MUSIC_DO               2
#define MUSIC_RE               3
#define MUSIC_MI               4
#define MUSIC_FA               5
#define MUSIC_SO               6
#define MUSIC_LA               7
#define MUSIC_XI               8

#define MUSIC_WARM             9
#define MUSIC_LIKE             10
#define MUSIC_CURIOUS          11
#define MUSIC_SUPRISED         12
#define MUSIC_ANGRY            13
#define MUSIC_CONFUSED         14
#define MUSIC_TIRED            15
#define MUSIC_MIO              16
#define MUSIC_DORAEMON         17
#define MUSIC_LONDON_BRIDGE    18
#define MUSIC_MERRY_CHRISTMAS  19
#define MUSIC_SMURFS           20
#define MUSIC_STOP             21

/* Accessing the embedded loudspeaker module with its predefined audio clips or
 * generating music notes, see the list above */

class ROBO3_OTPvoice : public ROBO3_MioBoard
{
	public:
		ROBO3_OTPvoice(void);
		void setpin_arduino(uint8_t resetPin, uint8_t dataPin);
		void setpin_mio(uint8_t port);
		void write(uint8_t part);

	private:
		uint8_t _DataPin;
		uint8_t _ResetPin;
		uint8_t _port;
};

#endif
