#ifndef ROBO3_CONFIG_H
#define ROBO3_CONFIG_H

#include <Arduino.h>

#define FALSE  0
#define TRUE   1

#define SUCCESS   1
#define UNSUCCESS 0

#define X      0
#define Y      1
#define Z      2

#define onBoardPort 0

#endif
