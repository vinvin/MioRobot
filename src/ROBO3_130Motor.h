#ifndef ROBO3_130MOTOR_H
#define ROBO3_130MOTOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_ModuleProtocol.h"

class ROBO3_130Motor : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_130Motor(void);
		void setpin_arduino(uint8_t port);
		void setpin_mio(uint8_t port, uint8_t num);
		void run(uint8_t stop_run);

	private:
		uint8_t _port;
		uint8_t Motor130_Pin;
		uint8_t Motor130_Group[3];
};

#endif
