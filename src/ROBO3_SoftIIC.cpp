#include "ROBO3_SoftIIC.h"  
#include "ROBO3_Config.h"

#define SCL_H  pinMode(SCL_PIN,INPUT)  //digitalWrite(SCL_PIN,HIGH)
#define SCL_L  pinMode(SCL_PIN,OUTPUT)  //digitalWrite(SCL_PIN,LOW)

#define SDA_H  pinMode(SDA_PIN,INPUT)
#define SDA_L  pinMode(SDA_PIN,OUTPUT)  //digitalWrite(SDA_PIN,LOW)

#define SCL_read      digitalRead(SCL_PIN) 
#define SDA_read      digitalRead(SDA_PIN) 

ROBO3_SoftIIC::ROBO3_SoftIIC()
{
	
}
void ROBO3_SoftIIC::masterInit(uint8_t sclPin, uint8_t sdaPin)
{
	SCL_PIN = sclPin;
	SDA_PIN = sdaPin;
	
    pinMode(SCL_PIN,INPUT);
    pinMode(SDA_PIN,INPUT);
	
	digitalWrite(SCL_PIN,LOW);
	digitalWrite(SDA_PIN,LOW);
}
void ROBO3_SoftIIC::IICdelay(void) 
{ 
    uint16_t i=5; //这里可以优化速度 ，经测试最低到5还能写入 
    while(i)  
    {  
        i--;  
    }  
}

int ROBO3_SoftIIC::start(void) 
{ 
    SDA_H; 
    SCL_H; 
    IICdelay(); 
    //if(!SDA_read) return FALSE; //SDA线为低电平则总线忙,退出 
    SDA_L; 
    IICdelay(); 
    //if(SDA_read) return FALSE; //SDA线为高电平则总线出错,退出 
    SDA_L; 
    IICdelay(); 
    return 1; 
} 

void ROBO3_SoftIIC::stop(void) 
{ 
    SCL_L; 
    IICdelay(); 
    SDA_L; 
    IICdelay(); 
    SCL_H; 
    IICdelay(); 
    SDA_H; 
    IICdelay(); 
} 

void ROBO3_SoftIIC::ack(void) 
{ 
    SCL_L; 
    IICdelay(); 
    SDA_L; 
    IICdelay(); 
    SCL_H; 
    IICdelay(); 
    SCL_L; 
    IICdelay(); 
} 

void ROBO3_SoftIIC::noAck(void) 
{ 
    SCL_L; 
    IICdelay(); 
    SDA_H; 
    IICdelay(); 
    SCL_H; 
    IICdelay(); 
    SCL_L; 
    IICdelay(); 
} 

int ROBO3_SoftIIC::waitAck(void)   //返回为:=1有ACK,=0无ACK 
{ 
    SCL_L; 
    IICdelay(); 
    SDA_H; 
    IICdelay(); 
    SCL_H; 
    IICdelay(); 
    if(SDA_read) 
    { 
        SCL_L; 
        return 0; 
    } 
    SCL_L; 
    return 1; 
} 

void ROBO3_SoftIIC::sendByte(uint8_t SendByte) //数据从高位到低位// 
{ 
    uint8_t i=8; 
    while(i--) 
    { 
        SCL_L; 
        IICdelay(); 
        if(SendByte&0x80) 
            SDA_H;   
        else  
            SDA_L;    
        SendByte<<=1; 
        IICdelay(); 
        SCL_H; 
        IICdelay(); 
    } 
    SCL_L; 
} 

uint8_t ROBO3_SoftIIC::receiveByte(void)  //数据从高位到低位// 
{  
    uint8_t i=8; 
    uint8_t ReceiveByte=0; 

    SDA_H; 
    while(i--) 
    { 
        ReceiveByte<<=1;       
        SCL_L; 
        IICdelay(); 
        SCL_H; 
        IICdelay(); 
        if(SDA_read) 
        { 
            ReceiveByte|=0x01; 
        } 
    } 
    SCL_L; 
    return ReceiveByte; 
} 

int ROBO3_SoftIIC::write(uint8_t sla_address, uint8_t sub_address, uint8_t data_w) 
{ 
    if (!start()) return 0; 
    sendByte(sla_address<<1);  
    if (!waitAck()) 
    { 
        //stop();  
        //return 0; 
    } 
    sendByte(sub_address);         
    waitAck(); 

    sendByte(data_w); 
    waitAck(); 

    stop(); 

    return 1; 
} 
         
uint8_t ROBO3_SoftIIC::read(uint8_t sla_address, uint8_t sub_address) 
{ 
    uint8_t data_r=0;

    if(!start()) return FALSE; 
    sendByte(sla_address<<1);  

    if(!waitAck())  
    { 
		//stop();  
		//return FALSE; 
    } 

    sendByte(sub_address);      
    waitAck(); 
    start(); 
    sendByte((sla_address<<1)+1); 
    waitAck(); 
    data_r = receiveByte(); 
    noAck(); 
    stop(); 
    return data_r; 
} 