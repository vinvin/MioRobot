#include "ROBO3_DS18B20.h"

ROBO3_DS18B20::ROBO3_DS18B20(void)
{
	
	ROBO3_ModuleProtocol::init();
}

void ROBO3_DS18B20::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	ROBO3_ModuleProtocol::init();
}

float ROBO3_DS18B20::read(uint8_t sensors)
{
	// uint8_t data=0;
	float temp;
	float tp;
	// data= 0x01<<sensors;
	// ROBO3_ModuleProtocol::sendData(0xfd, &data,1, _port);
	// delay(2);
	sensors = (2*sensors+8);
	temp = ROBO3_ModuleProtocol::receiveData_int(0xfd, sensors, _port);
/*    
	if(temp< 0)				//当温度值为负数
  	{
		temp=temp-1;
		temp=~((int16_t)temp);
		tp=temp;
		temp=tp*0.0625+0.5;	
		//留两个小数点就*100，+0.5是四舍五入，因为C语言浮点数转换为整型的时候把小数点
		//后面的数自动去掉，不管是否大于0.5，而+0.5之后大于0.5的就是进1了，小于0.5的就
		//算由?.5，还是在小数点后面。
  	}
 	else
  	{			
		tp=temp;//因为数据处理有小数点所以将温度赋给一个浮点型变量
		//如果温度是正的那么，那么正数的原码就是补码它本身
		temp=tp*0.0625+0.5;	
		//留两个小数点就*100，+0.5是四舍五入，因为C语言浮点数转换为整型的时候把小数点
		//后面的数自动去掉，不管是否大于0.5，而+0.5之后大于0.5的就是进1了，小于0.5的就
		//算加上0.5，还是在小数点后面。
	}
	*/
    return temp*0.01;
}
