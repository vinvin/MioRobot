#include "ROBO3_RemoteReceive.h"

ROBO3_RemoteReceive::ROBO3_RemoteReceive(void)
{
	
}



void ROBO3_RemoteReceive::ROBO3_readPackageByUART(void)
{
    static uint8_t index=0, packageStart=0, lastV=0;
    while(Serial.available())
    {
        value = Serial.read();
        if(packageStart == 0)
        {
            if(value==HEAD1&&lastV==HEAD0)//will start at next loop
            {
                index = 2;
                for(i=0;i<UARTGETBUF;i++)
                    uartGetBuf[i] = 0;
                uartGetBuf[0] = HEAD0;
                uartGetBuf[1] = HEAD1;
                packageStart = 1;
            } 
            else
            {
            lastV = value;
            }
        }
        else
        {
            uartGetBuf[index] = value;
            index++;
        }
        if(index >= uartGetBuf[DATALEN_INDEX]+DATALEN_INDEX+1)//end one package
        {
            packageStart = 0; 
            //buttonCmdMode = 0;
            //runUARTPackage();
            index = 0;
            break;
        } 
    }	
	
}


uint16_t  ROBO3_RemoteReceive::GetJoystick(uint8_t lr,uint8_t xy)
{
	ROBO3_readPackageByUART();
	if(lr==0&&xy==0)      return  (int16_t)(uartGetBuf[DEVICE_INDEX+2]<<8)|(int16_t)uartGetBuf[DEVICE_INDEX+1];
	else if(lr==0&&xy==1) return  (int16_t)(uartGetBuf[DEVICE_INDEX+4]<<8)|(int16_t)uartGetBuf[DEVICE_INDEX+3];
	else if(lr==1&&xy==0) return  (int16_t)(uartGetBuf[DEVICE_INDEX+6]<<8)|(int16_t)uartGetBuf[DEVICE_INDEX+5];
	else if(lr==1&&xy==1) return  (int16_t)(uartGetBuf[DEVICE_INDEX+8]<<8)|(int16_t)uartGetBuf[DEVICE_INDEX+7];
	else return 0;
}



uint8_t  ROBO3_RemoteReceive::Get_potentiometer(uint8_t LR)
{
	ROBO3_readPackageByUART();
	LT_Value=uartGetBuf[DEVICE_INDEX+9];
	
	RT_Value=uartGetBuf[DEVICE_INDEX+10];
	
	if(LR==0)return LT_Value;
	else     return RT_Value;	
}


/*************32位键值*****************/
/*uint32_t  ROBO3_RemoteReceive::Get_Key_Value(void)
{
	ROBO3_readPackageByUART();
	uint32_t keyValue=(uint32_t)(uartGetBuf[DEVICE_INDEX+10]<<24)|(uint32_t)(uartGetBuf[DEVICE_INDEX+9]<<16)|(uint32_t)(uartGetBuf[DEVICE_INDEX+8]<<8) | (uint32_t)uartGetBuf[DEVICE_INDEX+7];
	return  keyValue;
}*/



uint8_t  ROBO3_RemoteReceive::Get_Key_Value(void)
{
	ROBO3_readPackageByUART();
if(uartGetBuf[DEVICE_INDEX+13])return uartGetBuf[DEVICE_INDEX+13];
else if	(uartGetBuf[DEVICE_INDEX+14]) return uartGetBuf[DEVICE_INDEX+14]*3;
else return 0;
}

/*
uint8_t  ROBO3_RemoteReceive::Get_Keylbrb_Value(void)
{
	ROBO3_readPackageByUART();
if(uartGetBuf[DEVICE_INDEX+7])return uartGetBuf[DEVICE_INDEX+7];
else if	(uartGetBuf[DEVICE_INDEX+9]) return uartGetBuf[DEVICE_INDEX+9]*3;
else return 0;
	
}
*/


int8_t  ROBO3_RemoteReceive::Get_Anglevalue(uint8_t xy)
{
	ROBO3_readPackageByUART();
	if(xy==0)return  uartGetBuf[DEVICE_INDEX+11];
	else     return  uartGetBuf[DEVICE_INDEX+12];
	
}





























