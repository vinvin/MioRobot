#ifndef ROBO3_MIOBOARD_H
#define ROBO3_MIOBOARD_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"

#define HARDWARE_VERSION_ADDR	0

#define ULTRASONIC_ID		0x01
#define JOYSTICK_ID		0x02
#define LIGHTSENSOR_ID		0x03
#define POENTIOMETER_ID		0x04
#define MPU6050_ID		0x05
#define LIMIT_ID		0x06
#define SOUNDSENSOR_ID		0x07
#define RGBLED_ID		0x08
#define SegmentDisplay7_ID	0x09
#define DCMOTOR_ID		0x0a
#define SERVO_ID		0x0b
#define INFRAREDTRANSMIT_ID	0x0c
#define DS18B20_ID		0x0d
#define OTP_ID			0x0e
#define BATTERY_ID		0x0f
#define INFRAREDRECEIVER_ID	0x10
#define LINEFOLLOWER_ID		0x11
#define BUTTON_ID		0x12
#define EX_ANALOG_ID		0x13
#define COLORSENSOR_ID	 	0x14
#define EX_DIGITAL_ID		0x15
#define PIR_ID			0x16
#define KEY_ID			0x17
#define FLAME_ID		0x18
#define GRAY_ID			0x19
#define LEDMATRIX_ID		0x29
#define LCD_ID			0x20
#define MP3_ID			0x21
#define AI_ID			0x22
#define RGBLINE_ID		0x23
#define MOTOR130_ID		0x24
#define ENCODEMOTOR_ID		0x25

#define REMOTE_ID	0x80

#define MIO_M1		5
#define MIO_M2		6

class ROBO3_MioBoard
{
	public:
		ROBO3_MioBoard();
		void init();
		int16_t EEPROMread16bit(uint8_t addr);
		void EEPROMwrite16bit(uint8_t addr, uint16_t val);
		uint16_t version();

		uint8_t Mio_Button_Pin;
		uint8_t M1_INB_PIN;
		uint8_t M1_INA_PIN;
		uint8_t M2_INB_PIN;
		uint8_t M2_INA_PIN;
		uint8_t Mio_IR_Pin;
		uint8_t Mio_IT_Pin;
		uint8_t Mio_LightSensor_Pin;
		uint8_t Mio_OTPrst_Pin;
		uint8_t Mio_OTPdat_Pin;
		uint8_t Mio_RGBled_Pin;
		uint8_t Mio_Servo_Pin1;
		uint8_t Mio_Servo_Pin2;
		uint8_t Mio_SoundSensor_Pin;
		uint8_t MioBoardIO[5];

	private:
		uint16_t _version;
};

#endif
