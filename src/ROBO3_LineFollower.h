#ifndef ROBO3_LINEFOLLOWER_H
#define ROBO3_LINEFOLLOWER_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

#define ALL_SENSORS   0
#define LEFT_SENSOR   1
#define MEDIAN_SENSOR 2
#define RIGHT_SENSOR  3

#define LEFT_ADDRSS  4
#define MEDIAN_ADDRSS  6
#define RIGHT_ADDRSS  8

/* Accessing the line follower sensor */

class ROBO3_LineFollower : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_LineFollower(void);
		void setpin_mio(uint8_t port);
		uint8_t read(uint8_t sensors);
		uint16_t read_old(uint8_t sensors);
		void set_light_value(uint8_t address,uint16_t data);
	private:
		uint8_t  _port;
		//ROBO3_ModuleProtocol sensor;
};

#endif
