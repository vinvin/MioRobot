#include "ROBO3_PIR.h"

ROBO3_PIR::ROBO3_PIR(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_PIR::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	ROBO3_ModuleProtocol::init();
}

uint8_t ROBO3_PIR::read(void)
{
	uint8_t value;
	value = ROBO3_ModuleProtocol::receiveData_byte(PIR_ID, 0x01, _port);
	return value;
}

