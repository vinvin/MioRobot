#ifndef ROBO3_MP3_H
#define ROBO3_MP3_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"
#define PLAY 0x01
#define PAUS 0x02
#define NEXT 0x03
#define LAST 0x04
#define VOL_UP 0x05
#define VOL_DOWN 0x06
#define STOP 0x0E
#define PAUS_PLAY 0x0f

#define VOL 0x31
#define EQ 	0x32
#define CYCLE 0x33
#define U_TF 0x35
#define BUSY 0x38

/* Accessing the additional MP3 player module
 * http://miorobot.es/formacion/modulo-mp3
 */

class ROBO3_MP3 : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_MP3();
		void setpin_mio(uint8_t port);
		void MP3_STATE(uint8_t temp);
		void SET_MP3_MODE(uint8_t mode,uint8_t Value);
		void MP3_CHOICE(uint8_t mode,uint8_t Folder,uint8_t Value);
	private:
		uint8_t _port;
};

#endif
