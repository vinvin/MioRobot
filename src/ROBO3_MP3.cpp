#include "ROBO3_MP3.h"
//#include <Wire.h>

ROBO3_MP3::ROBO3_MP3(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_MP3::setpin_mio(uint8_t port)
{
	ROBO3_ModuleProtocol::init();
	
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
}


void ROBO3_MP3::MP3_STATE(uint8_t temp)
{
	uint8_t data[7] ={0x01,0x7e,0x02,0x00,0xef,0x0d,0x0a};
		data[3]=temp;
	ROBO3_ModuleProtocol::sendData(MP3_ID, data, 7, _port);
}
void ROBO3_MP3::SET_MP3_MODE(uint8_t mode,uint8_t Value)
{
	uint8_t data[8] ={0x01,0x7e,0x03,0x00,0x00,0xef,0x0d,0x0a};
		data[3]=mode;
		switch(mode)
		{
			case VOL :if(Value>30)Value=30;break;
			case EQ :if(Value>5)Value=5;break;
			case CYCLE:if(Value>4)Value=4;break;
			case U_TF :if(Value>1)Value=1;break;
			case BUSY :if(Value>1)Value=1;break;
		}
		data[4]=Value;
	ROBO3_ModuleProtocol::sendData(MP3_ID, data, 8, _port);
}
void ROBO3_MP3::MP3_CHOICE(uint8_t mode,uint8_t Folder,uint8_t Value)
{
	uint8_t data[9] ={0x01,0x7e,0x04,0x00,0x00,0x00,0xef,0x0d,0x0a};
		data[3]=mode;
		data[4]=Folder;
		data[5]=Value;
	ROBO3_ModuleProtocol::sendData(MP3_ID, data, 9, _port);
}

