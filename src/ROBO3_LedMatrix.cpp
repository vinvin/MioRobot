#include "ROBO3_LedMatrix.h"
//#include <Wire.h>

ROBO3_LedMatrix::ROBO3_LedMatrix(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_LedMatrix::setpin_mio(uint8_t port)
{
	ROBO3_ModuleProtocol::init();

	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
}

void ROBO3_LedMatrix::showNumber(int16_t number, uint8_t brightness)
{
	uint8_t data[4]={LED_MAT_MODE_NUMBER,(uint8_t)number, (uint8_t)(number>>8),brightness};
	ROBO3_ModuleProtocol::sendData(LEDMATRIX_ID, data, 4, _port);
}

void ROBO3_LedMatrix::showString(const char *str, uint8_t column, uint8_t brightness)
{
	uint8_t strLength, *data;
	for(strLength=0; str[strLength] != '\0'; strLength++);

	data = (uint8_t*)malloc(strLength+4);
	data[0] = LED_MAT_MODE_STRING;
	data[1] = strLength;
	for(uint8_t i=0; i<strLength; i++)
		data[i+2] = str[i];
	data[strLength+2] = column;	// X offset where the string will be drawn
	data[strLength+3] = brightness;

	ROBO3_ModuleProtocol::sendData(LEDMATRIX_ID, data, strLength+4, _port);

	free(data);
	data = NULL;
}

void ROBO3_LedMatrix::showBitMap(uint8_t* ledBuffer, uint8_t brightness)
{
	uint8_t data[18]={0};
	data[0] = LED_MAT_MODE_BITMAP;
	for(uint8_t i=0; i<16; i++)
		data[i+1] = ledBuffer[i];
	data[17] = brightness;

	ROBO3_ModuleProtocol::sendData(LEDMATRIX_ID, data, 18, _port);
	delay(4);
}

void ROBO3_LedMatrix::showWave(uint16_t value, uint8_t brightness)
{
	uint8_t data[4]={LED_MAT_MODE_WAVE, (uint8_t)value&0xff, (uint8_t)(value>>8)&0xff, brightness};

	ROBO3_ModuleProtocol::sendData(LEDMATRIX_ID, data, 4, _port);
}

void ROBO3_LedMatrix::getCommand(uint8_t* cmd, uint8_t length)
{
	uint8_t *data;
	data = (uint8_t*)malloc(length);

	for(uint8_t i=0;i<length;i++)
		data[i]=cmd[i];
	ROBO3_ModuleProtocol::sendData(LEDMATRIX_ID, data, length, _port);

	free(data);
	data = NULL;
}

void ROBO3_LedMatrix::showBit(uint8_t x, uint8_t y, uint8_t fill, uint8_t brightness)
{
	x=x-1;
	y=y-1;

	if(fill)
	{
		ledBufferpoint[_port-1][x]|=(0x80>>y);
	}
	else
	{
		ledBufferpoint[_port-1][x]&=(0xff-(0x80>>y));
	}
	showBitMap(ledBufferpoint[_port-1],brightness);
}

void ROBO3_LedMatrix::screenClear(void)
{
	for(uint8_t i=0;i<16;i++)
	{
		ledBufferpoint[_port-1][i]=0x00;
	}
	showBitMap(ledBufferpoint[_port-1],4);
}

uint8_t ROBO3_LedMatrix::read(uint8_t x, uint8_t y,uint8_t fill)
{
	uint8_t value;
	x=x-1;
	y=y-1;
	if(_port==1|_port==2|_port==3|_port==4)
	{
		value = 0x01 & (ledBufferpoint[_port-1][x] >> (7-y));
	}
	return value == fill;
}
