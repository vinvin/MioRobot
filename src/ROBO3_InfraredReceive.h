#ifndef ROBO3_INFRAREDRECEIVE_H
#define ROBO3_INFRAREDRECEIVE_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_Config.h"
#include "ROBO3_MioBoard.h"

/* Reading inputs from the infrared remote controller */

#if defined(__AVR_ATmega328P__)
#define D8_PCISR       PCINT0_vect
#define D9_PCISR       PCINT0_vect
#define D10_PCISR      PCINT0_vect
#define D11_PCISR      PCINT0_vect
#define D12_PCISR      PCINT0_vect
#define D13_PCISR      PCINT0_vect
#define D14_PCISR      PCINT1_vect
#define D15_PCISR      PCINT1_vect
#define D16_PCISR      PCINT1_vect
#define D17_PCISR      PCINT1_vect
#define D18_PCISR      PCINT1_vect
#define D19_PCISR      PCINT1_vect
#define D0_PCISR       PCINT2_vect
#define D1_PCISR       PCINT2_vect
#define D2_PCISR       PCINT2_vect
#define D3_PCISR       PCINT2_vect
#define D4_PCISR       PCINT2_vect
#define D5_PCISR       PCINT2_vect
#define D6_PCISR       PCINT2_vect
#define D7_PCISR       PCINT2_vect
#elif defined(__AVR_ATmega32U4__)
#define D17_PCISR      PCINT0_vect
#define D15_PCISR      PCINT0_vect
#define D16_PCISR      PCINT0_vect
#define D14_PCISR      PCINT0_vect
#define D8_PCISR       PCINT0_vect
#define D9_PCISR       PCINT0_vect
#define D10_PCISR      PCINT0_vect
#define D11_PCISR      PCINT0_vect
#endif

#define MARK  0
#define SPACE 1
#define NEC_BITS 32

#define USECPERTICK 50  // microseconds per clock interrupt tick
#define RAWBUF 100 // Length of raw duration buffer

#define NEC_HDR_MARK	9000
#define NEC_HDR_SPACE	4500
#define NEC_BIT_MARK	560
#define NEC_ONE_SPACE	1600
#define NEC_ZERO_SPACE	560
#define NEC_RPT_SPACE	2250
#define NEC_RPT_PERIOD	110000

// receiver states
#define STATE_IDLE     2
#define STATE_MARK     3
#define STATE_SPACE    4
#define STATE_STOP     5

// Values for decode_type
#define NEC 1
#define SONY 2
#define RC5 3
#define RC6 4
#define DISH 5
#define SHARP 6
#define PANASONIC 7
#define JVC 8
#define SANYO 9
#define MITSUBISHI 10
#define SAMSUNG 11
#define LG 12
#define UNKNOWN -1

#define BUTTON_NULL             0
#define BUTTON_SHUT_DOWN        1
#define BUTTON_MENU             2
#define BUTTON_SILENCE          3
#define BUTTON_MODE             4
#define BUTTON_PLUS             5
#define BUTTON_RETURN           6
#define BUTTON_FAST_BACKWARD    7
#define BUTTON_PLAY_PAUSE       8
#define BUTTON_FAST_FORWARD     9
#define BUTTON_ZERO             10
#define BUTTON_MINUS            11
#define BUTTON_OK               12
#define BUTTON_ONE              13
#define BUTTON_TWO              14
#define BUTTON_THREE            15
#define BUTTON_FOUR             16
#define BUTTON_FIVE             17
#define BUTTON_SIX              18
#define BUTTON_SEVEN            19
#define BUTTON_EIGHT            20
#define BUTTON_NINE             21
// information for the interrupt handler


// main class for receiving IR
class ROBO3_InfraredReceive : public ROBO3_MioBoard
{
	public:
		ROBO3_InfraredReceive();
		void setpin_mio(uint8_t port);
		void setpin_arduino(uint8_t pin);
		void init();
		void checkSignal();
		void dataUpdate();
		uint8_t read_u8(void);
		uint8_t read(void);
		uint8_t getPort();
		uint32_t read_u32(void);
		uint32_t read_u321(void);
	private:
		uint8_t irDataIndex;
		uint8_t newPackage;
		uint8_t _irPin;
		uint8_t _port;
		uint8_t button;
		uint32_t irData, irDataOld;
		uint32_t irReceiveTime, dataTimeNew, dataTimeOld;
		uint32_t irDecodeTime;

		bool match(uint32_t measured_ticks, uint32_t desired_us);
		void pin2PCMSK(uint8_t pin);
};

#endif
