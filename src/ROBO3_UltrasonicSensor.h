#ifndef ROBO3_ULTRASONICSENSOR_H
#define ROBO3_ULTRASONICSENSOR_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

/* Accessing the ultrasonic sensor */

class ROBO3_UltrasonicSensor : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_UltrasonicSensor(void);
		void setpin_mio(uint8_t port);
		uint16_t read();
	private:
		uint8_t tOut;
		uint8_t _port;
		uint32_t busyTime;//20171023 by gcy
		uint16_t valueLast;//20171023 by gcy
		//float distanceLast;
};

#endif
