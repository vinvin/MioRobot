#ifndef ROBO3_DS18B20_H
#define ROBO3_DS18B20_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

#define  DS18B20_ID  0x0d

/* Accessing a 1-Wire temperature sensor DS18B20 */

class ROBO3_DS18B20 : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_DS18B20(void);
		void setpin_mio(uint8_t port);
		float read(uint8_t sensors);
	private:
		uint8_t _port;
};

#endif
