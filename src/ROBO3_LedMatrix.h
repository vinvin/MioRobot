#ifndef ROBO3_LEDMATRIX_H
#define ROBO3_LEDMATRIX_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

/* Controlling the 8x16 LED matrix display of MIO, displaying numbers,
 * a waveform, a bitmap, or a string.
 * Could not find a complete documentation for the module's internal logic.
 *
 * Using the bitmap/ledbuffer logic, it is possible to display 6x8 or 3x8
 * characters as implemented in the low-level makeblock module:
 * https://github.com/Makeblock-official/Makeblock-Libraries/blob/master/src/MeLEDMatrixData.h
 *
 * Use http://dotmatrixtool.com/ to generate the 16 values for a bitmap
 */

#define LED_MAT_MODE_NUMBER	0x01
#define LED_MAT_MODE_STRING	0x02
#define LED_MAT_MODE_BITMAP	0x04
#define LED_MAT_MODE_WAVE	0x05

class ROBO3_LedMatrix : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_LedMatrix();
		void setpin_mio(uint8_t port);
		void showNumber(int16_t number, uint8_t brightness);
		void showString(const char *str, uint8_t column, uint8_t brightness);
		void showBitMap(uint8_t* ledBuffer, uint8_t brightness);
		void showBit(uint8_t x, uint8_t y, uint8_t fill, uint8_t brightness);
		void showWave(uint16_t value, uint8_t brightness);
		uint8_t read(uint8_t x, uint8_t y,uint8_t fill);
		void getCommand(uint8_t* cmd, uint8_t length);
		void screenClear(void);
	private:
		uint8_t _port;
		uint8_t ledBufferpoint[4][16]={
			// one buffer per possible port, each value is one column with 1 bit per pixel
			{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//描点
			{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//描点
			{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},//描点
			{0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00} //描点
		};
};

#endif
