#ifndef ROBO3_MPU6050_H
#define ROBO3_MPU6050_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include "ROBO3_ModuleProtocol.h"

#define MPU6050_ID     0x05

#define ANGLE_X  0
#define ANGLE_Y  1
#define ANGLE_Z  2
#define ACCX   3
#define ACCY   4
#define ACCZ   5
#define GYROX  6
#define GYROY  7
#define GYROZ  8

#ifndef X
#define X 0
#endif
#ifndef Y
#define Y 1
#endif
#ifndef Z
#define Z 2
#endif
#define ACC 1
#define GYRO 2

/* Accessing the additional accelerometer and gyrometer sensor module */

class ROBO3_MPU6050 : public ROBO3_ModuleProtocol
{
	public:
		ROBO3_MPU6050(void);
		void setpin_mio(uint8_t port);
		void read_buffer(uint8_t* buffer);
		short read(uint8_t sensors);
	private:
		uint8_t  _port;
};

#endif
