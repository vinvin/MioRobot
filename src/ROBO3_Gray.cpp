#include "ROBO3_Gray.h"
ROBO3_Gray::ROBO3_Gray(void)
{
	ROBO3_ModuleProtocol::init();
}

void ROBO3_Gray::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_sensorPin = pin;
	//ROBO3_ModuleProtocol::init(A5,A4,50);
}

void ROBO3_Gray::setpin_mio(uint8_t port)
{
	if(port < 1) port = 1;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		_sensorPin = Mio_LightSensor_Pin;
	}
	else
	{
	ROBO3_ModuleProtocol::init();
	}
}

uint16_t ROBO3_Gray::read()
{
	if(_port==1|_port==2|_port==3|_port==4)
	{
		value = ROBO3_ModuleProtocol::receiveData_int(0Xfe,GRAY_ID,_port);
	}
	return value;
}
