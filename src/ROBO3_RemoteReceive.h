#ifndef ROBO3_REMOTERECEIVE_H
#define ROBO3_REMOTERECEIVE_H

#include <stdint.h>
#include <stdbool.h>
#include <Arduino.h>
#include <Wire.h>
//#include "ROBO3_Config.h"

#define Wifi Serial
#define HEAD0               0xff
#define HEAD1               0x55
#define UARTGETBUF          32
#define DEVICE_INDEX        5
#define DATALEN_INDEX       2

class ROBO3_RemoteReceive
{
	public:
		ROBO3_RemoteReceive(void);
		void ROBO3_readPackageByUART(void);
		uint16_t  GetJoystick(uint8_t lr,uint8_t xy);
		uint8_t  Get_potentiometer(uint8_t LR);
		int8_t   Get_Anglevalue(uint8_t xy);
		uint8_t  Get_Key_Value(void);
		uint8_t  Get_Keylbrb_Value(void);
		//uint8_t  Get_LT_Value(void);
		//uint8_t  Get_RT_Value(void);
		//uint8_t  GetJoystick_L_X(void);
		//uint8_t  GetJoystick_L_Y(void);
		//uint8_t  GetJoystick_R_X(void);
		//uint8_t  GetJoystick_R_Y(void);
		//uint8_t  Get_Angle_Xvalue(void);
		//uint8_t  Get_Angle_Yvalue(void);
	private:
		uint8_t value=0, i=0;
		uint8_t uartGetBuf[UARTGETBUF]={0};
		uint8_t LT_Value,RT_Value;
		uint32_t keyValue = 0;
};

#endif
