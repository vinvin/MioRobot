#include "ROBO3_SoundSensor.h"

ROBO3_SoundSensor::ROBO3_SoundSensor(void)
{
	ROBO3_MioBoard::init();
}

void ROBO3_SoundSensor::setpin_arduino(uint8_t pin)
{
	_port = 0;
	_sensorPin = pin;
}

void ROBO3_SoundSensor::setpin_mio(uint8_t port)
{
	if(port < 0) port = 0;
	if(port > 4) port = 4;
	_port = port;
	
	if(_port == 0)
	{
		_sensorPin = Mio_SoundSensor_Pin;
	}
	else
	{
	}
}

uint16_t ROBO3_SoundSensor::read(void)
{
	uint8_t iicData[2],i=0;
	uint16_t value;
	if(_port == 0)
		value = analogRead(_sensorPin);
	else
	{
	}
	return value;
}
